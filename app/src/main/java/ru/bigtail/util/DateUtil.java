package ru.bigtail.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Павел on 06.08.2017.
 */

public class DateUtil {
    private static DateFormat f = new SimpleDateFormat("dd-m-yyyy");

    public static String getAge(String bdate) { // "31.5.1901"
        if (bdate == null || bdate.isEmpty())
            return "";

        String arr[]= new String[3];
        for (int i = 0; i < 3; i++)
            arr[i] = "";

        int it = 0;
        for (char c: bdate.toCharArray()) {
            if (c == '.')
                it++;
            else
                arr[it] += c;
        }


        if (arr[2] == null || arr[2].isEmpty()) {
            return ""; // когда год хз
        }

        String resSrt = arr[0] + "-" + (arr[1].length() < 2 ? ('0' + arr[1]) : arr[1])
                + "-" + arr[2];

        Date res;
        try {
            res = f.parse(resSrt);
        } catch (ParseException e) {
            return "";
        }

        long year = (new Date().getTime() - res.getTime()) / 31536000000L;

        switch ((int)(year % 10L)) {
            case 1:
                return year + " год";
            case 2:
                return year + " года";
            case 3:
                return year + " года";
            case 4:
                return year + " года";
            case 5:
                return year + " лет";
            case 6:
                return year + " лет";
            case 7:
                return year + " лет";
            case 8:
                return year + " лет";
            case 9:
                return year + " лет";
            case 0:
                return year + " лет";
        }

        return year + " лет";
    }
}
