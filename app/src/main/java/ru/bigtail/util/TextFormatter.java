package ru.bigtail.util;

import android.text.TextUtils;

import com.vk.sdk.api.model.VKApiUserFull;

import ru.bigtail.model.items.ModelUser;

public class TextFormatter {

    public static String getFormatInfo(VKApiUserFull user) {
        return String.format("%s%s%s", DateUtil.getAge(user.bdate),
                TextUtils.isEmpty(user.bdate) ? "" : ", ",
                user.city == null || TextUtils.isEmpty(user.city.title) ? "" : user.city.title);
    }

    public static String getFormatInfo(ModelUser modelUser) {
        String strInfo = DateUtil.getAge(modelUser.getBdate());
        if (modelUser.getCity() != null) {
            if (strInfo.isEmpty())
                strInfo += modelUser.getCity().getTitle();
            else
                strInfo += ", " + modelUser.getCity().getTitle();
        }
        return strInfo;
    }

    public static String getFormatCounterLikes(long countLikes) {
        if (countLikes >= 100000L) {
            return String.valueOf(countLikes / 100000L) + "M";
        }
        if (countLikes >= 1000L) {
            return String.valueOf(countLikes / 1000L) + "K";
        }
        return String.valueOf(countLikes);
    }

}
