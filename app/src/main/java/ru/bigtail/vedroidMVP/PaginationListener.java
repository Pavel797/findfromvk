package ru.bigtail.vedroidMVP;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.AbsListView;

public class PaginationListener extends RecyclerView.OnScrollListener {

    private LinearLayoutManager layoutManager;
    private boolean userScrolled = false;
    private LoadListener loadListener;

    public interface LoadListener {
        void loadMore();
    }

    public PaginationListener(LinearLayoutManager layoutManager, LoadListener loadListener) {
        this.layoutManager = layoutManager;
        this.loadListener = loadListener;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);

        if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
            userScrolled = true;
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

        if (userScrolled && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount && dy > 0) {
            userScrolled = false;
            loadListener.loadMore();
        }
    }
}
