package ru.bigtail.vedroidMVP.base.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import ru.bigtail.vedroidMVP.base.presenter.BasePresenter;
import ru.bigtail.vedroidMVP.base.presenter.PresenterManager;

public abstract class MVPActivity<P extends BasePresenter>  extends AppCompatActivity implements MVPView {

    protected P presenter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            presenter = getPresenterClass();
        } else {
            presenter = PresenterManager.getInstance().restorePresenter(savedInstanceState);
        }

        initUI();
    }

    public abstract P getPresenterClass();
    public abstract int getLayoutId();

    @Override
    protected void onResume() {
        super.onResume();
        presenter.bindView(this);
    }

    protected void initUI() {

    }

    @Override
    protected void onPause() {
        super.onPause();

        presenter.unbindView();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        PresenterManager.getInstance().savePresenter(presenter, outState);
    }
}
