package ru.bigtail.vedroidMVP.base.view;

public interface MVPView  {
    void showLoading();
    void hideLoading();
}
