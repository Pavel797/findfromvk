package ru.bigtail.vedroidMVP.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.bigtail.vedroidMVP.base.presenter.BasePresenter;
import ru.bigtail.vedroidMVP.base.view.MVPView;

public abstract class MvpViewHolder<P extends BasePresenter> extends RecyclerView.ViewHolder implements MVPView {

    protected P presenter;

    public MvpViewHolder(View itemView) {
        super(itemView);
    }

    public void bindPresenter(P presenter) {
        this.presenter = presenter;
        presenter.bindView(this);
    }

    public void unbindPresenter() {
        presenter = null;
    }
}