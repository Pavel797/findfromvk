package ru.bigtail.vedroidMVP.base.presenter;

import android.support.annotation.NonNull;

import java.lang.ref.WeakReference;

import ru.bigtail.vedroidMVP.base.view.MVPView;

public abstract class BasePresenter<M, V extends MVPView> {

    protected M model;
    private WeakReference<V> view;

    private boolean isLoadingData;

    public BasePresenter() {
        isLoadingData = false;
    }

    public void setModel(M model) {
        resetState();
        this.model = model;
        if (setupDone()) {
            updateView();
        }
    }

    protected void resetState() {
    }

    public void bindView(@NonNull V view) {
        this.view = new WeakReference<V>(view);
        if (setupDone()) {
            updateView();
        }
    }

    public void unbindView() {
        this.view = null;
    }

    protected V view() {
        if (view == null) {
            return null;
        } else {
            return view.get();
        }
    }

    protected abstract void updateView();

    protected boolean setupDone() {
        return view() != null && model != null;
    }

    public boolean isLoadingData() {
        return isLoadingData;
    }

    public void startLoadingData() {
        isLoadingData = true;
        view().showLoading();
    }

    public void finishLoadingData() {
        isLoadingData = false;
        view().hideLoading();
    }
}