package ru.bigtail.api.callback;

import ru.bigtail.model.answer.ModelAnswerUser;

/**
 * Created by Павел on 13.08.2017.
 */

public interface SetResultCallback {
    void setData(ModelAnswerUser data);

    void error();

    void hideProgressBar();

    void end();

    int getUsersCount();
}
