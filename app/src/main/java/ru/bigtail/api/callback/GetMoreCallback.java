package ru.bigtail.api.callback;

/**
 * Created by Павел on 04.08.2017.
 */

public interface GetMoreCallback {
    void getMore();
}
