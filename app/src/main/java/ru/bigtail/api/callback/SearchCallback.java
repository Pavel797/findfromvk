package ru.bigtail.api.callback;

import ru.bigtail.model.fields.DataFields;

/**
 * Created by Павел on 03.08.2017.
 */

public interface SearchCallback {
    void search(DataFields fields);
}
