package ru.bigtail.api.callback;

/**
 * Created by Павел on 03.08.2017.
 */

public interface CheckFieldsCallback {
    void goodFields();
    void errorFields();
}
