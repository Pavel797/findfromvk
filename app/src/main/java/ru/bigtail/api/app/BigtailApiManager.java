package ru.bigtail.api.app;

import android.net.Uri;
import android.os.SystemClock;

import com.vk.sdk.VKAccessToken;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Dispatcher;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.bigtail.api.vk.VkApiHelper;
import ru.bigtail.model.answer.ModelAnswerLike;
import ru.bigtail.model.answer.ModelAnswerUser;
import ru.bigtail.model.common.Types;
import ru.bigtail.model.fields.DataFields;
import ru.bigtail.model.fields.DataFieldsFriendsSearch;
import ru.bigtail.model.fields.DataFieldsGlobalSearch;
import ru.bigtail.model.fields.DataFieldsGroupSearch;
import ru.bigtail.model.history.ModelAnswerHistory;
import ru.bigtail.model.history.ModelAnswerHistoryDelete;
import ru.bigtail.model.profile.ProfileModelAnswer;

/**
 * Created by Павел on 02.08.2017.
 * <p>
 * Этот класс реализует паттерн singleton для работы с api из любого класса приложения
 * Здесь имеется явный вызов методов api с передачей параметров
 */

public class BigtailApiManager {

    private static BigtailApi bigtailApi;

    private BigtailApiManager() {
    }

    public static void init(String baseUrl) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                SystemClock.sleep(800);
                return chain.proceed(chain.request());
            }
        };

        builder.addNetworkInterceptor(interceptor);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        bigtailApi = retrofit.create(BigtailApi.class);
    }

    public static Call<ModelAnswerUser> search(DataFields fields, Callback<ModelAnswerUser> callback) {
        if (fields == null)
            throw new ExceptionInInitializerError("DataFields must be initialize");

        switch (fields.getTypeFields()) {
            case Types.TYPE_FIELDS_GLOBAL:
                return globalSearch((DataFieldsGlobalSearch) fields, callback);
            case Types.TYPE_FIELDS_FRIENDS:
                return friendsSearch((DataFieldsFriendsSearch) fields, callback);
            case Types.TYPE_FIELDS_GROUP:
                return groupSearch((DataFieldsGroupSearch) fields, callback);
        }

        throw new ExceptionInInitializerError("DataFields type not found");
    }

    private static String getPath(String url) {
        String res = Uri.parse(url).getPath();
        res = res.trim();
        res = res.replace("/", "");
        return res;
    }

    private static Call<ModelAnswerUser> globalSearch(DataFieldsGlobalSearch fields, Callback<ModelAnswerUser> callback) {
        Map<String, Object> parametersMap = new HashMap<>();
        parametersMap.put("fields", VkApiHelper.fields_get);
        if (fields.getName() != null)
            parametersMap.put("q", fields.getName());
        if (fields.getSex() != null)
            parametersMap.put("sex", fields.getSex());
        if (fields.getAgeFrom() != null)
            parametersMap.put("age_from", fields.getAgeFrom());
        if (fields.getAgeTo() != null)
            parametersMap.put("age_to", fields.getAgeTo());
        if (fields.getCityId() != null)
            parametersMap.put("city", fields.getCityId());
        if (fields.getCountriesId() != null)
            parametersMap.put("country", fields.getCountriesId());
        if (fields.getHistoryId() != null)
            parametersMap.put("history_id", fields.getHistoryId());

        parametersMap.put("ignore_history", fields.isIgnoreHistory());

        String token = VKAccessToken.currentToken().accessToken,
                userId = VKAccessToken.currentToken().userId;

        Call<ModelAnswerUser> res = bigtailApi.globalSearch(token, userId, VkApiHelper.count_get,
                (long) fields.getOffset(), parametersMap);

        res.enqueue(callback);

        return res;
    }

    private static Call<ModelAnswerUser> groupSearch(DataFieldsGroupSearch fields, Callback<ModelAnswerUser> callback) {
        Map<String, Object> parametersMap = new HashMap<>();
        parametersMap.put("fields", VkApiHelper.fields_get);

        if (fields.getLink() != null)
            parametersMap.put("screen_name", getPath(fields.getLink()));
        if (fields.getName() != null)
            parametersMap.put("q", fields.getName());
        if (fields.getSex() != null)
            parametersMap.put("sex", fields.getSex());
        if (fields.getAgeFrom() != null)
            parametersMap.put("age_from", fields.getAgeFrom());
        if (fields.getAgeTo() != null)
            parametersMap.put("age_to", fields.getAgeTo());
        if (fields.getCityId() != null)
            parametersMap.put("city", fields.getCityId());
        if (fields.getCountriesId() != null)
            parametersMap.put("country", fields.getCountriesId());
        if (fields.getHistoryId() != null)
            parametersMap.put("history_id", fields.getHistoryId());

        parametersMap.put("ignore_history", fields.isIgnoreHistory());


        String token = VKAccessToken.currentToken().accessToken,
                userId = VKAccessToken.currentToken().userId;

        Call<ModelAnswerUser> res = bigtailApi.groupSearch(token, userId, VkApiHelper.count_get,
                (long) fields.getOffset(), parametersMap);

        res.enqueue(callback);

        return res;
    }

    private static Call<ModelAnswerUser> friendsSearch(DataFieldsFriendsSearch fields, Callback<ModelAnswerUser> callback) {
        Map<String, Object> parametersMap = new HashMap<>();
        parametersMap.put("fields", VkApiHelper.fields_get);
        if (fields.getName() != null)
            parametersMap.put("q", fields.getName());
        if (fields.getLink() != null)
            parametersMap.put("screen_name", getPath(fields.getLink()));
        if (fields.getHistoryId() != null)
            parametersMap.put("history_id", fields.getHistoryId());

        parametersMap.put("ignore_history", fields.isIgnoreHistory());

        String token = VKAccessToken.currentToken().accessToken,
                userId = VKAccessToken.currentToken().userId;

        Call<ModelAnswerUser> res = bigtailApi.friendsSearch(token, userId, VkApiHelper.count_get,
                (long) fields.getOffset(), parametersMap);

        res.enqueue(callback);

        return res;
    }

    public static Call<ModelAnswerLike> likesClick(Long likeTo, Callback<ModelAnswerLike> callback) {

        String token = VKAccessToken.currentToken().accessToken,
                userId = VKAccessToken.currentToken().userId;

        Call<ModelAnswerLike> res = bigtailApi.likesClick(token, userId, likeTo);

        res.enqueue(callback);

        return res;
    }

    public static Call<ModelAnswerUser> whoLikeMe(Long offset, Callback<ModelAnswerUser> callback) {

        String token = VKAccessToken.currentToken().accessToken,
                userId = VKAccessToken.currentToken().userId;

        Call<ModelAnswerUser> res = bigtailApi.whoLikeMe(token, userId, VkApiHelper.count_get, offset, VkApiHelper.fields_get);

        res.enqueue(callback);

        return res;
    }

    public static Call<ModelAnswerHistory> historyGetList(Long offset, Callback<ModelAnswerHistory> callback) {

        String token = VKAccessToken.currentToken().accessToken,
                userId = VKAccessToken.currentToken().userId;

        Call<ModelAnswerHistory> res = bigtailApi.historyGetList(token, userId, VkApiHelper.count_get, offset);
        res.enqueue(callback);

        return res;
    }

    public static Call<ModelAnswerHistoryDelete> historyDelete(String history_id, Callback<ModelAnswerHistoryDelete> callback) {

        String token = VKAccessToken.currentToken().accessToken,
                userId = VKAccessToken.currentToken().userId;

        Call<ModelAnswerHistoryDelete> res = bigtailApi.historyDelete(token, userId, history_id);
        res.enqueue(callback);

        return res;
    }

    public static Call<ProfileModelAnswer> getProfile(Callback<ProfileModelAnswer> callback) {
        String token = VKAccessToken.currentToken().accessToken,
                userId = VKAccessToken.currentToken().userId;

        Call<ProfileModelAnswer> res = bigtailApi.getProfile(token, userId);

        res.enqueue(callback);

        return res;
    }

    public static Call<ModelAnswerUser> meLikes(long offset, Callback<ModelAnswerUser> callback) {
        String token = VKAccessToken.currentToken().accessToken,
                userId = VKAccessToken.currentToken().userId;

        Call<ModelAnswerUser> res = bigtailApi.meLikes(token, userId, VkApiHelper.count_get, offset);

        res.enqueue(callback);

        return res;
    }

    public static Call<ModelAnswerUser>  getFriendsListWithLikes(long offset, Callback<ModelAnswerUser> callback) {
        String token = VKAccessToken.currentToken().accessToken,
                userId = VKAccessToken.currentToken().userId;

        Call<ModelAnswerUser> res = bigtailApi.getFriendsListWithLikes(token, userId, VkApiHelper.count_get, offset);

        res.enqueue(callback);

        return res;
    }
}
