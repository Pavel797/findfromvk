package ru.bigtail.api.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;

import com.yandex.metrica.YandexMetrica;

import ru.bigtail.R;

/**
 * Created by Павел on 13.08.2017.
 *
 * Класс для обработаки ошибок
 */

public class BigtailErrorHelper {

    private static Context context;

    private BigtailErrorHelper() {
    }

    public static void init(Context cont) {
        context = cont;
    }

    public static boolean error(int error_code, String error_message) {
        String eventParameters = "{\"error_message\":" + error_message + ", \"error_code\":"+ error_code +"}";

        switch (error_code) {
            case 0:
                return true;
            case 2:
                Toast.makeText(context, R.string.invalid_reply_from_vk_api, Toast.LENGTH_LONG).show();
                break;
            case 4:
                Toast.makeText(context, R.string.user_id_or_access_token_is_not_received, Toast.LENGTH_SHORT).show();
                break;
            case 7:
                Toast.makeText(context, R.string.user_is_not_premium, Toast.LENGTH_LONG).show();
                break;
            case 8:
                Toast.makeText(context, R.string.invalid_request_check_request_url, Toast.LENGTH_SHORT).show();
                break;
            case 12:
                Toast.makeText(context, R.string.connection_to_DB_error, Toast.LENGTH_LONG).show();
                break;
            case 13:
                Toast.makeText(context, R.string.missed_required_params, Toast.LENGTH_SHORT).show();
                break;
            case 15:
                Toast.makeText(context, R.string.invalid_access_token_for_this_user_id, Toast.LENGTH_SHORT).show();
                break;
            case 17:
                Toast.makeText(context, R.string.history_with_received_id_is_not_exist, Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(context, R.string.unknown_error, Toast.LENGTH_SHORT).show();
        }

        YandexMetrica.reportEvent("Bigtail api error", eventParameters);

        return false;
    }

    public static void requestNotSuccessful(int code, String message) {
        String eventParameters = "{\"error_message\":" + message + ", \"code\":"+ code +"}";
        YandexMetrica.reportEvent("Request not successful", eventParameters);
    }

    public static void onFailure(Throwable t) {
        Toast.makeText(context, R.string.network_error, Toast.LENGTH_SHORT).show();
    }
}
