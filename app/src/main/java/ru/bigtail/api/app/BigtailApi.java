package ru.bigtail.api.app;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import ru.bigtail.model.answer.ModelAnswerLike;
import ru.bigtail.model.answer.ModelAnswerUser;
import ru.bigtail.model.history.ModelAnswerHistory;
import ru.bigtail.model.history.ModelAnswerHistoryDelete;
import ru.bigtail.model.profile.ProfileModelAnswer;

/**
 * Created by Павел on 31.07.2017.
 * <p>
 * Это интерфейс с базовыми методами api
 */

public interface BigtailApi {

    @GET("search.global")
    Call<ModelAnswerUser> globalSearch(@Query("access_token") String accessToken,
                                       @Query("user_id") String userId,
                                       @Query("count") Long count,
                                       @Query("offset") Long offset,
                                       @QueryMap Map<String, Object> fields);

    @GET("search.friend")
    Call<ModelAnswerUser> friendsSearch(@Query("access_token") String accessToken,
                                        @Query("user_id") String userId,
                                        @Query("count") Long count,
                                        @Query("offset") Long offset,
                                        @QueryMap Map<String, Object> fields);

    @GET("search.group")
    Call<ModelAnswerUser> groupSearch(@Query("access_token") String accessToken,
                                      @Query("user_id") String userId,
                                      @Query("count") Long count,
                                      @Query("offset") Long offset,
                                      @QueryMap Map<String, Object> fields);

    @GET("likes.click")
    Call<ModelAnswerLike> likesClick(@Query("access_token") String accessToken,
                                     @Query("user_id") String userId,
                                     @Query("like_to") Long likeTo);

    @GET("likes.whoLikeMe")
    Call<ModelAnswerUser> whoLikeMe(@Query("access_token") String accessToken,
                                    @Query("user_id") String userId,
                                    @Query("count") Long count,
                                    @Query("offset") Long offset,
                                    @Query("fields") String fields);

    @GET("history.getList")
    Call<ModelAnswerHistory> historyGetList(@Query("access_token") String accessToken,
                                            @Query("user_id") String userId,
                                            @Query("count") Long count,
                                            @Query("offset") Long offset);

    @GET("history.delete")
    Call<ModelAnswerHistoryDelete> historyDelete(@Query("access_token") String accessToken,
                                                 @Query("user_id") String userId,
                                                 @Query("history_id") String history_id);

    @GET("profile.getInfo")
    Call<ProfileModelAnswer> getProfile(@Query("access_token") String accessToken,
                                        @Query("user_id") String userId);

    @GET("likes.meLikes")
    Call<ModelAnswerUser> meLikes(@Query("access_token") String accessToken,
                                  @Query("user_id") String userId,
                                  @Query("count") Long count,
                                  @Query("offset") Long offset);

    @GET("likes.getFriendsListWithLikes")
    Call<ModelAnswerUser> getFriendsListWithLikes(@Query("access_token") String accessToken,
                                                  @Query("user_id") String userId,
                                                  @Query("count") Long count,
                                                  @Query("offset") Long offset);
}
