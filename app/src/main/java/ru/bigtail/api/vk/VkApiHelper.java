package ru.bigtail.api.vk;

import android.net.Uri;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKScopes;

import java.util.HashMap;
import java.util.Map;

import ru.bigtail.api.callback.CheckFieldsCallback;
import ru.bigtail.model.answer.ModelAnswerCheckLink;

/**
 * Created by Павел on 13.07.2017.
 */

public class VkApiHelper {

    //public static final int ID_APP = -1;
    public static final String API_VERSION = "5.65";

    public static final String[] scope = {VKScopes.GROUPS, VKScopes.FRIENDS, VKScopes.PHOTOS, VKScopes.OFFLINE};

    public static final String fields_get = "bdate,city,country,screen_name,online,photo_50,photo_100,photo_200_orig,photo_200,photo_400_orig,photo_max,photo_max_orig";

    public static final long count_get  = 10L;

    public static void logout() {
        VKSdk.logout();
    }

    private static String getPath(String url) {
        String res = Uri.parse(url).getPath();
        res = res.trim();
        res = res.replace("/", "");
        return res;
    }

    public static void getMyGroups(Long offset, VKRequest.VKRequestListener listener) {
        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("offset", offset);
        parametersMap.put("count", count_get);
        parametersMap.put("extended", 1);
        parametersMap.put("fields", fields_get);

        VKRequest request = VKApi.groups().get(new VKParameters(parametersMap));

        request.executeWithListener(listener);
    }

    public static void getMyFriends(Long offset, VKRequest.VKRequestListener listener) {
        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("offset", offset);
        parametersMap.put("count", count_get);
        parametersMap.put("hints", "hints");
        parametersMap.put("fields", fields_get);

        VKRequest request = VKApi.friends().get(new VKParameters(parametersMap));

        request.executeWithListener(listener);
    }

    public static void getCountries(int count, int offset, VKRequest.VKRequestListener listener) {
        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("count", count);
        parametersMap.put("offset", offset);

        VKRequest request = new VKRequest("database.getCountries", new VKParameters(parametersMap));

        request.executeWithListener(listener);
    }

    public static void getCities(String q, int countryId, VKRequest.VKRequestListener listener) {
        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("q", q);
        parametersMap.put("country_id", countryId);
        parametersMap.put("count", 40);
        parametersMap.put("need_all", 1);

        VKRequest request = new VKRequest("database.getCities", new VKParameters(parametersMap));

        request.executeSyncWithListener(listener);
    }

    public static void checkLink(String url, CheckFieldsCallback callback, String type) {
        CheckLinkTask checkLinkTask = new CheckLinkTask(callback, type);
        checkLinkTask.execute(url);
    }

    private static class CheckLinkTask extends AsyncTask<String, Void, Boolean> {

        private CheckFieldsCallback callback;
        private String type;

        CheckLinkTask(CheckFieldsCallback callback, String type) {
            this.callback = callback;
            this.type = type;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            final Boolean[] res = {false};

            Map<String, Object> parametersMap = new HashMap<>();
            parametersMap.put("screen_name", getPath(params[0]));
            VKRequest request = new VKRequest("utils.resolveScreenName", new VKParameters(parametersMap));
            request.executeSyncWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    GsonBuilder builder = new GsonBuilder();
                    Gson gson = builder.create();
                    ModelAnswerCheckLink answer = null;
                    try {
                        answer = gson.fromJson(response.json.toString(), ModelAnswerCheckLink.class);
                    } catch (Exception e) {
                        res[0] = false;
                    }

                    if (answer ==  null || answer.getResponse() == null ||
                            !answer.getResponse().getType().equals(type)) {
                        res[0] = false;
                    } else {
                        res[0] = true;
                    }

                    super.onComplete(response);
                }

                @Override
                public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                    res[0] = false;
                    super.attemptFailed(request, attemptNumber, totalAttempts);
                }

                @Override
                public void onError(VKError error) {
                    res[0] = false;
                    super.onError(error);
                }
            });

            return res[0];
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            super.onPostExecute(aVoid);
            if (aVoid)
                callback.goodFields();
            else
                callback.errorFields();
        }
    }
}
