package ru.bigtail.api.vk;

import android.content.Context;
import android.widget.Toast;

import com.vk.sdk.api.VKError;

import ru.bigtail.R;

/**
 * Created by Павел on 14.07.2017.
 */

public class VkErrorHelper {

    public static void error(VKError error, Context context) {
        switch (error.errorCode) {
            case -102:
                break;
            case -105:
                Toast.makeText(context, context.getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                break;
            case -101:
                Toast.makeText(context, context.getString(R.string.very_many_reqests), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(context, context.getString(R.string.fatal_error) + " " + String.valueOf(error.errorCode), Toast.LENGTH_LONG).show();
        }
    }

}
