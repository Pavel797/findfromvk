package ru.bigtail.ui.adapter.list;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.LongSparseArray;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.bigtail.api.app.BigtailErrorHelper;
import ru.bigtail.model.activity.SearchActivity;
import ru.bigtail.model.answer.ModelAnswerLike;
import ru.bigtail.model.items.ModelUser;
import ru.bigtail.R;
import ru.bigtail.api.app.BigtailApiManager;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.model.common.Types;
import ru.bigtail.model.fields.DataFieldsFriendsSearch;
import ru.bigtail.model.items.CustomCardItem;
import ru.bigtail.model.items.ModelProgressBar;
import ru.bigtail.util.TextFormatter;

/**
 * Created by Павел on 04.08.2017.
 */

public class UserListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CustomCardItem> items;
    private LongSparseArray<Boolean> itemsMap;
    private SearchActivity activity;
    private boolean isEnd, isPB;
    private View progressBar;
    private Call callClickLike;

    public UserListAdapter(SearchActivity activity) {
        items = new ArrayList<>();
        itemsMap = new LongSparseArray<>();
        isPB = false;
        isEnd = false;
        this.activity = activity;
        progressBar = null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Types.TYPE_CARD) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_list_item, parent, false);
            return new ItemHolderCard(v);
        } else if (viewType == Types.TYPE_PROGRESS_BAR) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.model_progres_bar, parent, false);

            progressBar = v;

            ProgressBar modelPB = (ProgressBar) v.findViewById(R.id.modelPB);

            return new ItemProgressBarHolder(v, modelPB);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final CustomCardItem customItem = items.get(position);

        if (customItem.getTypeView() == Types.TYPE_CARD) {
            final ItemHolderCard itemViewHolder = (ItemHolderCard) holder;
            final ModelUser modelUser = (ModelUser) customItem;
            holder.itemView.setEnabled(true);

            if (modelUser.getIs_liked()) {
                itemViewHolder.counterTotalLikeTV.setTextColor(itemViewHolder.itemView.getResources().getColor(R.color.red));
                itemViewHolder.likeIV.setImageDrawable(ContextCompat.getDrawable(itemViewHolder.itemView.getContext(), R.drawable.ic_heart_active));
            } else {
                itemViewHolder.counterTotalLikeTV.setTextColor(itemViewHolder.itemView.getResources().getColor(R.color.whiteGrey));
                itemViewHolder.likeIV.setImageDrawable(ContextCompat.getDrawable(itemViewHolder.itemView.getContext(), R.drawable.ic_heart_passive));
            }

            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.linkProfileAIV:
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(v.getResources().getString(R.string.baseVkURL) + modelUser.getScreen_name()));
                            activity.startActivity(intent);
                            break;
                        case R.id.likeIV:
                            itemViewHolder.likeIV.startAnimation(AnimationUtils.loadAnimation(itemViewHolder.itemView.getContext(), R.anim.rotate_center));
                            callClickLike = BigtailApiManager.likesClick(modelUser.getId(), new Callback<ModelAnswerLike>() {
                                @Override
                                public void onResponse(Call<ModelAnswerLike> call, Response<ModelAnswerLike> response) {
                                    if (response.isSuccessful() && response.body() != null) {
                                        ModelAnswerLike answer = response.body();
                                        if (BigtailErrorHelper.error(answer.getErrorCode(), answer.getErrorMessage())) {
                                            if (answer.getResponse().getIsLiked()) {
                                                itemViewHolder.likeIV.setImageDrawable(ContextCompat.getDrawable(itemViewHolder.itemView.getContext(), R.drawable.ic_heart_active));
                                            } else {
                                                itemViewHolder.likeIV.setImageDrawable(ContextCompat.getDrawable(itemViewHolder.itemView.getContext(), R.drawable.ic_heart_passive));
                                            }
                                        }
                                        itemViewHolder.counterTotalLikeTV.setText(String.valueOf(response.body().getResponse().getLikesCountTotal()));
                                    } else {
                                        BigtailErrorHelper.requestNotSuccessful(response.code(), response.message());
                                    }

                                    itemViewHolder.likeIV.clearAnimation();
                                    YoYo.with(Techniques.Pulse)
                                            .duration(500)
                                            .playOn(itemViewHolder.likeIV);
                                }

                                @Override
                                public void onFailure(Call<ModelAnswerLike> call, Throwable t) {
                                    if (!call.isCanceled()) {
                                        itemViewHolder.likeIV.clearAnimation();
                                        BigtailErrorHelper.onFailure(t);
                                    }
                                }
                            });
                            break;
                        case R.id.linkInFormACIV:
                            activity.openForm(new DataFieldsFriendsSearch(v.getResources().getString(R.string.baseVkURL) + modelUser.getScreen_name(), ""));
                            break;
                    }
                }
            };

            itemViewHolder.linkInFormACIV.setOnClickListener(onClickListener);
            itemViewHolder.linkProfileAIV.setOnClickListener(onClickListener);
            itemViewHolder.likeIV.setOnClickListener(onClickListener);

            if (modelUser.getLinksPhoto() == null && modelUser.getPhoto_max_orig() == null) {
                itemViewHolder.rvPhotos.setVisibility(View.GONE);
            } else {
                itemViewHolder.rvPhotos.setVisibility(View.VISIBLE);
                itemViewHolder.adapter.addItem(modelUser.getLinksPhoto(), modelUser.getPhoto_max_orig());
            }

            itemViewHolder.counterTotalLikeTV.setText(TextFormatter.getFormatCounterLikes(modelUser.getLikes_count_total()));
            itemViewHolder.nameTV.setText(modelUser.getFirst_name() + " " + modelUser.getLast_name());
            itemViewHolder.infoTV.setText(modelUser.getInfo());

            Picasso.with(holder.itemView.getContext())
                    .load(modelUser.getPhoto_100())
                    .memoryPolicy(MemoryPolicy.NO_STORE, MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                    .fit()
                    //.centerInside()
                    .placeholder(AppCompatDrawableManager.get().getDrawable(holder.itemView.getContext(), R.drawable.ic_more_horiz))
                    .error(AppCompatDrawableManager.get().getDrawable(holder.itemView.getContext(), R.drawable.ic_error_outline))
                    .into(itemViewHolder.profilePhotoIV);

        } else if (customItem.getTypeView() == Types.TYPE_PROGRESS_BAR) {
            ItemProgressBarHolder itemHolder = (ItemProgressBarHolder) holder;

            if (isEnd)
                itemHolder.modelPB.setVisibility(View.GONE);
            else {
                itemHolder.modelPB.setVisibility(View.VISIBLE);
            }
        }
    }

    public void setEnd(boolean val) {
        isEnd = val;
        if (progressBar != null && val) {
            progressBar.setVisibility(View.GONE);
            notifyItemChanged(items.size() - 1);
        } else if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
            notifyItemChanged(items.size() - 1);
        }
    }

    public void clear() {
        if (getItemCount() != 0) {
            itemsMap.clear();
            isPB = false;
            setEnd(false);
            items.clear();
            items = new ArrayList<>();
            notifyDataSetChanged();
        }
    }

    private CustomCardItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public boolean onFailedToRecycleView(RecyclerView.ViewHolder holder) {
        return true;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getTypeView();
    }

    public void addUserCard(ArrayList<ModelUser> userCards) {
        int sizeNewItem = 0;
        for (ModelUser i : userCards) {
            if (itemsMap.get(i.getId()) == null) {
                sizeNewItem++;
                itemsMap.put(i.getId(), true);
                if (!isPB)
                    items.add(i);
                else
                    items.add(getItemCount() - 1, i);
            }
        }

        if (!isPB) {
            items.add(new ModelProgressBar());
            isPB = true;
        }

        notifyItemRangeInserted(items.size() - sizeNewItem, sizeNewItem);
    }

    public long getOffset() {
        if (isPB)
            return items.size() - 1;
        return items.size();
    }

    public void detachView() {
        if (callClickLike != null && !callClickLike.isCanceled() && !callClickLike.isExecuted())
            callClickLike.cancel();
    }

    public class ItemHolderCard extends RecyclerView.ViewHolder {

        @BindView(R.id.nameTV)
        TextView nameTV;

        @BindView(R.id.profilePhotoIV)
        CircleImageView profilePhotoIV;

        @BindView(R.id.rvPhotos)
        RecyclerView rvPhotos;

        @BindView(R.id.linkProfileAIV)
        AppCompatImageView linkProfileAIV;

        @BindView(R.id.linkInFormACIV)
        AppCompatImageView linkInFormACIV;

        @BindView(R.id.counterTotalLikeTV)
        TextView counterTotalLikeTV;

        @BindView(R.id.likeIV)
        AppCompatImageView likeIV;

        @BindView(R.id.infoTV)
        TextView infoTV;

        PhotoAdapter adapter;

        ItemHolderCard(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            nameTV.setTypeface(AssetsManager.openSansRegular);
            counterTotalLikeTV.setTypeface(AssetsManager.openSansRegular);
            infoTV.setTypeface(AssetsManager.openSansRegular);

            adapter = new PhotoAdapter();
            rvPhotos.setAdapter(adapter);
            rvPhotos.setLayoutManager(new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
        }
    }

    private static class ItemProgressBarHolder extends RecyclerView.ViewHolder {
        ProgressBar modelPB;

        ItemProgressBarHolder(View itemView, ProgressBar modelPB) {
            super(itemView);
            this.modelPB = modelPB;
        }
    }
}
