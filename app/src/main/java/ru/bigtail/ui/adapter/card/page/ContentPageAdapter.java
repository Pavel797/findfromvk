package ru.bigtail.ui.adapter.card.page;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.R;

/**
 * Created by Павел on 02.08.2017.
 */

public class ContentPageAdapter extends PagerAdapter {

    @BindView(R.id.photoIV)
    RoundedImageView photoIV;

    @BindView(R.id.openIV)
    AppCompatImageView openIV;

    private List<String> linksPhoto = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    private Context context;

    public ContentPageAdapter() {
    }

    public void attach(Context context) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View rootView = mLayoutInflater.inflate(R.layout.page_content, container, false);
        ButterKnife.bind(this, rootView);

        Picasso.with(rootView.getContext())
                .load(linksPhoto.get(position))
                .memoryPolicy(MemoryPolicy.NO_STORE, MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                .placeholder(R.drawable.ic_more_horiz)
                .error(R.drawable.ic_error_outline)
                .into(photoIV);

        openIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImageViewer.Builder<>(context, linksPhoto)
                        .setStartPosition(position)
                        .show();
            }
        });

        container.addView(rootView);

        return rootView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return linksPhoto.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (View) object;
    }

    public void addLink(String link) {
        linksPhoto.add(link);
    }

    public void clear() {
        linksPhoto.clear();
    }

}
