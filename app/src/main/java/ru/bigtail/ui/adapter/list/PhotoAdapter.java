package ru.bigtail.ui.adapter.list;

import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.ArrayList;
import java.util.List;

import ru.bigtail.R;
import ru.bigtail.model.items.ModelPhoto;

/**
 * Created by Павел on 14.07.2017.
 */

public class PhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ModelPhoto> items;
    private List<String> listLinks;

    public PhotoAdapter() {
        items = new ArrayList<>();
        listLinks = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.model_photo_item, parent, false);

        AppCompatImageView photoIV = (AppCompatImageView) v.findViewById(R.id.photoIV);

        return new ItemHolderPhoto(v, photoIV);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        ModelPhoto modelPhoto = items.get(position);
        final ItemHolderPhoto itemHolderPhoto = (ItemHolderPhoto) holder;

        itemHolderPhoto.photoIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImageViewer.Builder<>(itemHolderPhoto.itemView.getContext(), listLinks)
                        .setStartPosition(holder.getAdapterPosition())
                        .show();
            }
        });

        if (modelPhoto.getHeight() != -1 && modelPhoto.getWidth() != -1) {
            itemHolderPhoto.photoIV.getLayoutParams().width = (modelPhoto.getWidth() * itemHolderPhoto.photoIV.getLayoutParams().height) /
                    (modelPhoto.getHeight() == 0 ? itemHolderPhoto.photoIV.getLayoutParams().height : modelPhoto.getHeight());
            itemHolderPhoto.photoIV.requestLayout();
        }

        Picasso.with(holder.itemView.getContext())
                .load(listLinks.get(position))
                .memoryPolicy(MemoryPolicy.NO_STORE, MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                .fit().centerInside()
                .placeholder(AppCompatDrawableManager.get().getDrawable(holder.itemView.getContext(), R.drawable.ic_more_horiz))
                .error(AppCompatDrawableManager.get().getDrawable(holder.itemView.getContext(), R.drawable.ic_error_outline))
                .into(itemHolderPhoto.photoIV);
    }


    @Override
    public boolean onFailedToRecycleView(RecyclerView.ViewHolder holder) {
        return true;
    }

    public void addItem(ArrayList<ModelPhoto> photos, String avatar) {
        items.clear();
        listLinks.clear();
        if (avatar != null && !avatar.isEmpty()) {
            ModelPhoto avatarPhoto = new ModelPhoto(avatar, 400, 400);
            items.add(avatarPhoto);
            listLinks.add(avatarPhoto.getPhoto_604());
        }
        if (photos != null && photos.size() != 0) {
            for (ModelPhoto i : photos) {
                items.add(i);
                listLinks.add(i.getPhoto_604());
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private static class ItemHolderPhoto extends RecyclerView.ViewHolder {

        AppCompatImageView photoIV;

        ItemHolderPhoto(View itemView, AppCompatImageView photoIV) {
            super(itemView);
            this.photoIV = photoIV;
        }
    }
}
