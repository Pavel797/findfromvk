package ru.bigtail.ui.adapter.card.card;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.bigtail.model.items.ModelUser;
import ru.bigtail.api.callback.GetMoreCallback;
import ru.bigtail.view.CardSliderView;

/**
 * Created by Павел on 02.08.2017.
 */

public class ContentCardAdapter extends PagerAdapter {

    private List<ModelUser> userCards;
    private GetMoreCallback getMoreCallback;
    private Activity activity;

    public ContentCardAdapter(Activity activity) {
        userCards = new ArrayList<>();
        this.activity = activity;
    }

    @Override
    public View instantiateItem(ViewGroup collection, int position) {
        CardSliderView sliderView = new CardSliderView(activity, collection);
        sliderView.setModelUserCard(userCards.get(position));
        collection.addView(sliderView.getView());
        if (position + 1 >= userCards.size())
            getMoreCallback.getMore();
        return sliderView.getView();
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((View) arg1);
    }

    @Override
    public int getCount() {
        return userCards.size();
    }

    public void clear() {
        this.userCards.clear();
        notifyDataSetChanged();
    }

    public void addUserCard(ArrayList<ModelUser> items) {
        for (ModelUser i: items) {
            userCards.add(i);
        }
        notifyDataSetChanged();
    }

    public int getSize() {
        return userCards.size();
    }

    public void setLoadMoreCallback(GetMoreCallback getMoreCallback) {
        this.getMoreCallback = getMoreCallback;
    }
}
