package ru.bigtail.ui.adapter.friendslike;

import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ru.bigtail.R;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.model.items.ModelUser;
import ru.bigtail.util.TextFormatter;

/**
 * Created by Павел on 23.08.2017.
 */

public class FriendLikeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ModelUser> items;

    public FriendLikeAdapter() {
        items = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemHolderUser(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_friend_like, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final ModelUser modelUser = items.get(position);
        final ItemHolderUser itemHolderPhoto = (ItemHolderUser) holder;

        Picasso.with(holder.itemView.getContext())
                .load(modelUser.getPhoto_100())
                .memoryPolicy(MemoryPolicy.NO_STORE, MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                .fit()
                .placeholder(AppCompatDrawableManager.get().getDrawable(holder.itemView.getContext(), R.drawable.ic_more_horiz))
                .error(AppCompatDrawableManager.get().getDrawable(holder.itemView.getContext(), R.drawable.ic_error_outline))
                .into(itemHolderPhoto.photoCIV);

        itemHolderPhoto.nameTV.setText(modelUser.getFirst_name() + " " + modelUser.getLast_name());
        itemHolderPhoto.infoTV.setText(modelUser.getInfo());

        itemHolderPhoto.likesCountManTV.setText(TextFormatter.getFormatCounterLikes(modelUser.getLikesCountMan()));
        itemHolderPhoto.likesCountWomenTVTV.setText(TextFormatter.getFormatCounterLikes(modelUser.getLikesCountWomen()));
    }


    @Override
    public boolean onFailedToRecycleView(RecyclerView.ViewHolder holder) {
        return true;
    }

    public void addItem(ArrayList<ModelUser> users) {
        if (users != null && users.size() != 0) {
            for (ModelUser i : users) {
                items.add(i);
            }

            notifyItemRangeInserted(items.size() - users.size(), users.size());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setEnd(boolean end) {
    }

    public long getOffset() {
        return items.size();
    }

    public void clear() {
        items.clear();
    }

    public static class ItemHolderUser extends RecyclerView.ViewHolder {

        @BindView(R.id.photoCIV)
        CircleImageView photoCIV;

        @BindView(R.id.nameTV)
        TextView nameTV;

        @BindView(R.id.infoTV)
        TextView infoTV;

        @BindView(R.id.likesCountManTV)
        TextView likesCountManTV;

        @BindView(R.id.likesCountWomenTVTV)
        TextView likesCountWomenTVTV;

        ItemHolderUser(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            nameTV.setTypeface(AssetsManager.openSansRegular);
            infoTV.setTypeface(AssetsManager.openSansRegular);
        }
    }
}