package ru.bigtail.ui.adapter.forms;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.util.ArrayList;
import java.util.List;

import ru.bigtail.api.vk.VkApiHelper;
import ru.bigtail.model.answer.ModelAnswerCity;
import ru.bigtail.model.forms.ModelCity;

/**
 * Created by Павел on 16.07.2017.
 */

public class CitiesAutoCompleteAdapter extends BaseAdapter implements Filterable {

    private LayoutInflater layoutInflater;
    private List<ModelCity> mResults;
    private int countryId;

    public CitiesAutoCompleteAdapter(LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
        mResults = new ArrayList<>();
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    @Override
    public int getCount() {
        return mResults.size();
    }

    @Override
    public ModelCity getItem(int index) {
        return mResults.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        }
        ModelCity city = getItem(position);
        TextView text1 = (TextView) convertView.findViewById(android.R.id.text1);

        SpannableString ss = new SpannableString(city.getTitle());
        if (city.getImportant() == 1)
            ss.setSpan(new UnderlineSpan(), 0, city.getTitle().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text1.setText(ss);

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    List<ModelCity> city = findCity(constraint.toString());
                    // Assign the data to the FilterResults
                    filterResults.values = city;
                    filterResults.count = city.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    mResults = (List<ModelCity>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};
    }

    private List<ModelCity> findCity(String q) {
        final List<ModelCity> res = new ArrayList<>();
        if (countryId < 0)
            return res;

        VkApiHelper.getCities(q, countryId, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);

                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                ModelAnswerCity answer = gson.fromJson(response.json.toString(), ModelAnswerCity.class);

                for (ModelCity i: answer.getResponse().getItems()) {
                    res.add(i);
                }
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                super.attemptFailed(request, attemptNumber, totalAttempts);
            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
            }
        });

        return res;
    }
}
