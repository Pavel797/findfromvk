package ru.bigtail.ui.adapter.friend;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiUserFull;

import ru.bigtail.R;
import ru.bigtail.model.common.Types;
import ru.bigtail.util.TextFormatter;
import ru.bigtail.vedroidMVP.base.presenter.BasePresenter;

public class FriendPresenter extends BasePresenter<VKApiUserFull, ItemHolderUser> {
    @Override
    protected void updateView() {

        view().friendLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                //intent.putExtra(Types.link, itemHolderPhoto.itemView.getContext().getString(R.string.baseVkURL) + user.id);
                //activity.setResult(Activity.RESULT_OK, intent);
                //activity.finish();
            }
        });

        Picasso.with(view().itemView.getContext())
                .load(model.photo_100)
                .memoryPolicy(MemoryPolicy.NO_STORE, MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                .fit()
                .placeholder(R.drawable.ic_more_horiz)
                .error(R.drawable.ic_error_outline)
                .into(view().photoCIV);

        view().nameTV.setText(String.format("%s, %s", model.first_name, model.last_name));
        view().infoTV.setText(TextFormatter.getFormatInfo(model));
    }
}
