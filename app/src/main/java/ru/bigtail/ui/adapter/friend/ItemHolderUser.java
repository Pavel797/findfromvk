package ru.bigtail.ui.adapter.friend;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vk.sdk.api.model.VKApiUserFull;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ru.bigtail.R;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.vedroidMVP.base.MvpViewHolder;
import ru.bigtail.vedroidMVP.base.presenter.BasePresenter;
import ru.bigtail.vedroidMVP.base.view.MVPView;

public class ItemHolderUser extends MvpViewHolder<BasePresenter> implements MVPView {

    @BindView(R.id.photoCIV)
    CircleImageView photoCIV;

    @BindView(R.id.nameTV)
    TextView nameTV;

    @BindView(R.id.infoTV)
    TextView infoTV;

    @BindView(R.id.friendLL)
    LinearLayout friendLL;

    ItemHolderUser(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        nameTV.setTypeface(AssetsManager.openSansRegular);
        infoTV.setTypeface(AssetsManager.openSansRegular);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
