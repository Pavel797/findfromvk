package ru.bigtail.ui.adapter.group;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.model.common.Types;
import ru.bigtail.R;
import ru.bigtail.model.group.ModelGroup;
import ru.bigtail.ui.viewers.group.GroupListActivity;

/**
 * Created by Павел on 18.08.2017.
 */

public class GroupAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ModelGroup> items;
    private GroupListActivity activity;

    public GroupAdapter(GroupListActivity activity) {
        items = new ArrayList<>();
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_group, parent, false);

        return new ItemHolderGroup(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final ModelGroup modelGroup = items.get(position);
        final ItemHolderGroup itemHolderGroup = (ItemHolderGroup) holder;

        itemHolderGroup.groupLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(Types.link, itemHolderGroup.itemView.getContext().getString(R.string.baseVkURL) + modelGroup.getScreen_name());
                activity.setResult(Activity.RESULT_OK, intent);
                activity.finish();
            }
        });

        Picasso.with(holder.itemView.getContext())
                .load(modelGroup.getPhoto_100())
                .memoryPolicy(MemoryPolicy.NO_STORE, MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                .fit()
                .placeholder(AppCompatDrawableManager.get().getDrawable(holder.itemView.getContext(), R.drawable.ic_more_horiz))
                .error(AppCompatDrawableManager.get().getDrawable(holder.itemView.getContext(), R.drawable.ic_error_outline))
                .into(itemHolderGroup.photoCIV);

        itemHolderGroup.nameTV.setText(modelGroup.getName());
        itemHolderGroup.infoTV.setText(null);
    }


    @Override
    public boolean onFailedToRecycleView(RecyclerView.ViewHolder holder) {
        return true;
    }

    public void addItem(ModelGroup[] groups) {
        if (groups != null && groups.length != 0) {
            for (ModelGroup i : groups) {
                items.add(i);
            }

            notifyItemRangeInserted(items.size() - groups.length, groups.length);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setEnd(boolean end) {
    }

    public long getOffset() {
        return items.size();
    }

    public void clear() {
        items.clear();
    }

    public static class ItemHolderGroup extends RecyclerView.ViewHolder {

        @BindView(R.id.photoCIV)
        CircleImageView photoCIV;

        @BindView(R.id.nameTV)
        TextView nameTV;

        @BindView(R.id.infoTV)
        TextView infoTV;

        @BindView(R.id.groupLL)
        LinearLayout groupLL;

        ItemHolderGroup(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            nameTV.setTypeface(AssetsManager.openSansRegular);
            infoTV.setTypeface(AssetsManager.openSansRegular);
        }
    }
}