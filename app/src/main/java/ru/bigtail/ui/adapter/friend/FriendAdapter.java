package ru.bigtail.ui.adapter.friend;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.vk.sdk.api.model.VKApiUserFull;

import ru.bigtail.R;
import ru.bigtail.vedroidMVP.base.MvpRecyclerListAdapter;
import ru.bigtail.vedroidMVP.base.presenter.BasePresenter;

public class FriendAdapter extends MvpRecyclerListAdapter<VKApiUserFull, BasePresenter, ItemHolderUser> {

    @Override
    public ItemHolderUser onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemHolderUser(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_friend, parent, false));
    }

    @NonNull
    @Override
    protected BasePresenter createPresenter(@NonNull VKApiUserFull model) {
        FriendPresenter presenter = new FriendPresenter();
        presenter.setModel(model);
        return presenter;
    }

    @NonNull
    @Override
    protected Object getModelId(@NonNull VKApiUserFull model) {
        return model.id;
    }
}
