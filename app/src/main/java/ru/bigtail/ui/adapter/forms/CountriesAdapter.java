package ru.bigtail.ui.adapter.forms;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ru.bigtail.model.forms.ModelCountry;

/**
 * Created by Павел on 16.07.2017.
 */

public class CountriesAdapter extends ArrayAdapter<ModelCountry> {

    private LayoutInflater layoutInflater;

    public CountriesAdapter(LayoutInflater layoutInflater, int textViewResourceId, List<ModelCountry> objects) {
        super(layoutInflater.getContext(), textViewResourceId, objects);
        this.layoutInflater = layoutInflater;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {

        return getCustomView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        return getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        View simple_dropdown_item_1line = layoutInflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        TextView text1 = (TextView) simple_dropdown_item_1line.findViewById(android.R.id.text1);
        text1.setText(getItem(position).getTitle());
        return simple_dropdown_item_1line;
    }
}
