package ru.bigtail.ui.adapter.history;

import android.app.Activity;
import android.content.res.Resources;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.bigtail.api.app.BigtailErrorHelper;
import ru.bigtail.model.common.Types;
import ru.bigtail.model.fields.DataFieldsGlobalSearch;
import ru.bigtail.model.fields.DataFieldsGroupSearch;
import ru.bigtail.model.history.HistoryObject;
import ru.bigtail.ui.main.activity.MainActivity;
import ru.bigtail.R;
import ru.bigtail.api.app.BigtailApiManager;
import ru.bigtail.model.Item;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.model.fields.DataFields;
import ru.bigtail.model.fields.DataFieldsFriendsSearch;
import ru.bigtail.model.history.ModelAnswerHistoryDelete;
import ru.bigtail.model.history.ModelHistory;
import ru.bigtail.model.items.ModelProgressBar;

/**
 * Created by Павел on 13.08.2017.
 */

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Item> items;
    private MainActivity activity;
    private boolean isEnd, isPB;
    private View progressBar;
    private Call<ModelAnswerHistoryDelete> callHistoryDelete;

    public HistoryAdapter(Activity activity) {
        items = new ArrayList<>();
        isPB = false;
        isEnd = false;
        this.activity = (MainActivity) activity;
        progressBar = null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Types.TYPE_HISTORY) {
            return new ItemHolderHistory(parent);
        } else if (viewType == Types.TYPE_PROGRESS_BAR) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.model_progres_bar, parent, false);

            progressBar = v;

            ProgressBar modelPB = (ProgressBar) v.findViewById(R.id.modelPB);

            return new ItemProgressBarHolder(v, modelPB);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Item customItem = items.get(position);

        if (customItem.getTypeView() == Types.TYPE_HISTORY) {
            final ItemHolderHistory itemViewHolder = (ItemHolderHistory) holder;
            final ModelHistory modelHistory = (ModelHistory) customItem;
            final Resources resources = itemViewHolder.itemView.getResources();
            holder.itemView.setEnabled(true);

            switch (modelHistory.getType()) {
                case Types.Type_global:
                    itemViewHolder.typeSearchTV.setText(R.string.global_search_type);
                    itemViewHolder.screenNameTV.setText(null);
                    break;
                case Types.Type_group:
                    itemViewHolder.typeSearchTV.setText(R.string.group_search_type);
                    itemViewHolder.screenNameTV.setText(modelHistory.getHistoryObject().getScreenName());
                    break;
                case Types.Type_friend:
                    itemViewHolder.typeSearchTV.setText(R.string.friend_search_type);
                    itemViewHolder.screenNameTV.setText(modelHistory.getHistoryObject().getScreenName());
                    break;
            }

            long date = modelHistory.getDate() * 1000L;
            DateFormat df = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT);
            itemViewHolder.dataTV.setText(df.format(date));

            if (modelHistory.getHistoryObject().getQ() != null && !modelHistory.getHistoryObject().getQ().isEmpty()) {
                itemViewHolder.qTV.setText(String.valueOf(resources.getString(R.string.name_s_l_hint) + " " + modelHistory.getHistoryObject().getQ()));
            } else {
                itemViewHolder.qTV.setText(null);
            }

            //itemViewHolder.ageFromTV.setVisibility(View.GONE);
            //itemViewHolder.ageToTV.setVisibility(View.GONE);

            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.deleteACIV:
                            itemViewHolder.deleteACIV.startAnimation(AnimationUtils.loadAnimation(itemViewHolder.itemView.getContext(), R.anim.rotate_center));
                            callHistoryDelete = BigtailApiManager.historyDelete(modelHistory.getHistoryId(), new Callback<ModelAnswerHistoryDelete>() {
                                @Override
                                public void onResponse(Call<ModelAnswerHistoryDelete> call, Response<ModelAnswerHistoryDelete> response) {
                                    if (response.isSuccessful() && response.body() != null) {
                                        ModelAnswerHistoryDelete answer = response.body();
                                        if (answer.isDeleted() && BigtailErrorHelper.error(answer.getErrorCode(), answer.getErrorMessage())) {
                                            remove(itemViewHolder.getAdapterPosition());
                                        }
                                    } else {
                                        BigtailErrorHelper.requestNotSuccessful(response.code(), response.message());
                                    }
                                    itemViewHolder.deleteACIV.clearAnimation();
                                }

                                @Override
                                public void onFailure(Call<ModelAnswerHistoryDelete> call, Throwable t) {
                                    if (!call.isCanceled()) {
                                        itemViewHolder.deleteACIV.clearAnimation();
                                        BigtailErrorHelper.onFailure(t);
                                    }
                                }
                            });
                            break;
                        case R.id.editACIV:
                            openForm(modelHistory.getType(), modelHistory.getHistoryObject());
                            break;
                        case R.id.reSearchACIV:
                            reSearch(modelHistory.getType(), modelHistory.getHistoryObject(),
                                    modelHistory.getOffset(), modelHistory.getHistoryId());
                            break;
                    }
                }
            };

            itemViewHolder.editACIV.setOnClickListener(onClickListener);
            itemViewHolder.reSearchACIV.setOnClickListener(onClickListener);
            itemViewHolder.deleteACIV.setOnClickListener(onClickListener);

        } else if (customItem.getTypeView() == Types.TYPE_PROGRESS_BAR) {
            ItemProgressBarHolder itemHolder = (ItemProgressBarHolder) holder;

            if (isEnd)
                itemHolder.modelPB.setVisibility(View.GONE);
            else {
                itemHolder.modelPB.setVisibility(View.VISIBLE);
            }
        }
    }

    private void openForm(String type, HistoryObject historyObject) {
        activity.openForm(getFields(type, historyObject));
    }

    private void reSearch(String type, HistoryObject historyObject, int offset, String historyId) {
        DataFields dataFields = getFields(type, historyObject);
        dataFields.setOffset(offset);
        dataFields.setHistoryId(historyId);
        activity.search(dataFields);
    }

    private DataFields getFields(String type, HistoryObject historyObject) {
        DataFields resFields = null;

        switch (type) {
            case Types.Type_global:
                resFields = new DataFieldsGlobalSearch(historyObject.getSex(),
                        historyObject.getQ(), historyObject.getAge_from(), historyObject.getAge_to(),
                        historyObject.getCity(), historyObject.getCountry());
                break;
            case Types.Type_group:
                resFields = new DataFieldsGroupSearch(historyObject.getSex(),
                        historyObject.getQ(), String.valueOf(activity.getString(R.string.baseVkURL) + historyObject.getScreenName()),
                        historyObject.getAge_from(), historyObject.getAge_to(),
                        historyObject.getCity(), historyObject.getCountry());
                break;
            case Types.Type_friend:
                resFields = new DataFieldsFriendsSearch(
                        String.valueOf(activity.getString(R.string.baseVkURL) + historyObject.getScreenName()),
                        historyObject.getQ());
                break;
        }
        return resFields;
    }

    public void remove(int location) {
        if (location >= items.size())
            return;

        items.remove(location);
        notifyItemRemoved(location);
    }

    public void setEnd(boolean val) {
        isEnd = val;
        if (progressBar != null && val) {
            progressBar.setVisibility(View.GONE);
            notifyItemChanged(items.size() - 1);
        } else if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
            notifyItemChanged(items.size() - 1);
        }
    }

    public void clear() {
        if (getItemCount() != 0) {
            isPB = false;
            setEnd(false);
            items.clear();
            items = new ArrayList<>();
            notifyDataSetChanged();
        }
    }

    @Override
    public boolean onFailedToRecycleView(RecyclerView.ViewHolder holder) {
        return true;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public int getOffset() {
        if (isPB)
            return items.size() - 1;
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getTypeView();
    }

    public void addHistory(ArrayList<ModelHistory> histories) {
        if (histories == null)
            return;

        for (ModelHistory i : histories) {
            if (!isPB)
                items.add(i);
            else
                items.add(getItemCount() - 1, i);
        }

        notifyItemRangeInserted(items.size() - histories.size(), histories.size());

        if (!isPB) {
            items.add(new ModelProgressBar());
            isPB = true;
        }
    }

    public void detachView() {
        if (callHistoryDelete != null && !callHistoryDelete.isCanceled() && !callHistoryDelete.isExecuted())
            callHistoryDelete.cancel();
    }

    public class ItemHolderHistory extends RecyclerView.ViewHolder {

        @BindView(R.id.typeSearchTV)
        TextView typeSearchTV;

        @BindView(R.id.dataTV)
        TextView dataTV;

        @BindView(R.id.screenNameTV)
        TextView screenNameTV;

        @BindView(R.id.qTV)
        TextView qTV;

        /*@BindView(R.id.ageFromTV)
        TextView ageFromTV;

        @BindView(R.id.ageToTV)
        TextView ageToTV;*/

        @BindView(R.id.deleteACIV)
        AppCompatImageView deleteACIV;

        @BindView(R.id.editACIV)
        AppCompatImageView editACIV;

        @BindView(R.id.reSearchACIV)
        AppCompatImageView reSearchACIV;

        ItemHolderHistory(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_history_item, parent, false));
            ButterKnife.bind(this, itemView);

            typeSearchTV.setTypeface(AssetsManager.openSansLight);
            dataTV.setTypeface(AssetsManager.openSansLight);
            screenNameTV.setTypeface(AssetsManager.openSansLight);
            qTV.setTypeface(AssetsManager.openSansLight);
        }
    }

    private static class ItemProgressBarHolder extends RecyclerView.ViewHolder {
        ProgressBar modelPB;

        ItemProgressBarHolder(View itemView, ProgressBar modelPB) {
            super(itemView);
            this.modelPB = modelPB;
        }
    }
}
