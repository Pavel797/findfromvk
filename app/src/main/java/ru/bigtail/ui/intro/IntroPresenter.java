package ru.bigtail.ui.intro;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.widget.Button;
import android.widget.TextView;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import ru.bigtail.R;
import ru.bigtail.api.vk.VkApiHelper;
import ru.bigtail.api.vk.VkErrorHelper;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.model.preferences.PreferencesHelper;

/**
 * Created by Павел on 28.07.2017.
 */

public class IntroPresenter {

    private IntroActivity view;

    public IntroPresenter() {
    }

    public void attachView(IntroActivity view) {
        this.view = view;
    }

    public void detachView() {
        this.view = null;
    }

    public void login() {
        VKSdk.login(view, VkApiHelper.scope);
    }

    public void showAuthorizationNoteDialog() {
        AlertDialog.Builder ad = new AlertDialog.Builder(view);
        ad.setTitle(R.string.authorization_note);  // заголовок
        ad.setMessage(R.string.message_dialog_authorization); // сообщение

        ad.setPositiveButton(R.string.understand, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();
            }
        });

        AlertDialog dialog = ad.show();

        TextView messageTV = (TextView) dialog.findViewById(android.R.id.message);
        if (messageTV != null)
            messageTV.setTypeface(AssetsManager.openSansRegular);

        TextView titleTV = (TextView) dialog.findViewById(R.id.alertTitle);
        if (titleTV != null)
            titleTV.setTypeface(AssetsManager.openSansLight);

        Button button1 = (Button) dialog.findViewById(android.R.id.button1);
        if (button1 != null)
            button1.setTypeface(AssetsManager.openSansRegular);

        Button button2 = (Button) dialog.findViewById(android.R.id.button2);
        if (button2 != null)
            button2.setTypeface(AssetsManager.openSansRegular);
    }

    private void successLogin() {
        PreferencesHelper.setFirstApplicationLaunch();
        view.finish();
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        return  (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                successLogin();
            }

            @Override
            public void onError(VKError error) {
                VkErrorHelper.error(error, view.getApplicationContext());
            }
        }));
    }
}
