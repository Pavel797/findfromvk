package ru.bigtail.ui.intro;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.R;

public class IntroActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.singInLL)
    LinearLayout singInLL;

    @BindView(R.id.authorizationNoteTV)
    TextView authorizationNoteTV;

    @BindView(R.id.singInTV)
    TextView singInTV;

    @BindView(R.id.contentTV)
    TextView contentTV;

    @BindView(R.id.titleTV)
    TextView titleTV;

    private IntroPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        presenter = new IntroPresenter();
        presenter.attachView(this);

        initUI();

    }

    private void initUI() {
        singInTV.setTypeface(AssetsManager.openSansLight);
        authorizationNoteTV.setTypeface(AssetsManager.openSansLight);
        contentTV.setTypeface(AssetsManager.openSansLight);
        titleTV.setTypeface(AssetsManager.openSansLight);

        singInLL.setOnClickListener(this);

        String strAuthorizationNoteTV = getResources().getString(R.string.authorization_note);
        SpannableString ss =new SpannableString(strAuthorizationNoteTV);
        ss.setSpan(new UnderlineSpan(), 0, strAuthorizationNoteTV.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        authorizationNoteTV.setText(ss);
        authorizationNoteTV.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.singInLL:
                presenter.login();
                break;
            case R.id.authorizationNoteTV:
                presenter.showAuthorizationNoteDialog();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (presenter.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
