package ru.bigtail.ui.main.fragments.results;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.model.fields.DataFields;
import ru.bigtail.model.preferences.PreferencesHelper;
import ru.bigtail.R;

public class ResultsFragment extends Fragment {

    @BindView(R.id.container)
    LinearLayout container;

    @BindView(R.id.resultsNullLL)
    LinearLayout resultsNullLL;

    @BindView(R.id.resultsNullTV)
    TextView resultsNullTV;

    private ResultsPresenter presenter;

    public ResultsFragment() {
        presenter = new ResultsPresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_results, container, false);
        ButterKnife.bind(this, rootView);
        presenter.attachView(this);

        initUI();

        return rootView;
    }

    private void initUI() {
        resultsNullTV.setTypeface(AssetsManager.openSansRegular);

        setHasOptionsMenu(true);

        View thisView = getView();
        if (thisView != null)
            thisView.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fadein));

        presenter.viewResult();
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();

        getActivity().setTitle(R.string.results_title);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.view_results_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemCards:
                if (PreferencesHelper.isViewInList()) {
                    PreferencesHelper.viewInCards();
                    presenter.viewInCards();
                    return true;
                }
                break;
            case R.id.itemList:
                if (PreferencesHelper.isViewInCards()) {
                    PreferencesHelper.viewInList();
                    presenter.viewInList();
                    return true;
                }
                break;
            default:
                return false;
        }
        return false;
    }

    public void setDataFields(DataFields dataFields) {
        this.presenter.setDataFields(dataFields);
    }

    public void setNullData(boolean val) {
        if (val) {
            resultsNullLL.setVisibility(View.VISIBLE);
        } else {
            resultsNullLL.setVisibility(View.GONE);
        }
    }
}
