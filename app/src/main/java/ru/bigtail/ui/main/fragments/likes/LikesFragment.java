package ru.bigtail.ui.main.fragments.likes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.R;
import ru.bigtail.model.assets.AssetsManager;

public class LikesFragment extends Fragment {

    @BindView(R.id.meLikeRV)
    RecyclerView meLikeRV;

    @BindView(R.id.meLikeSRL)
    SwipeRefreshLayout meLikeSRL;

    @BindView(R.id.likeNullLL)
    LinearLayout likeNullLL;

    @BindView(R.id.likeNullTV)
    TextView likeNullTV;

    private SwipeRefreshLayout.OnRefreshListener refreshListener;
    private LinearLayoutManager layoutManager;
    private LikesPresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_likes, container, false);
        ButterKnife.bind(this, rootView);

        presenter = new LikesPresenter();
        presenter.attachView(this);

        initUI();

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle(R.string.mi_likes);
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    private void initUI() {
        likeNullTV.setTypeface(AssetsManager.openSansRegular);

        refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.onRefresh();
            }
        };

        meLikeSRL.setOnRefreshListener(refreshListener);
        layoutManager = new LinearLayoutManager(getActivity());
        meLikeRV.setLayoutManager(layoutManager);
        meLikeRV.setAdapter(presenter.getAdapter());
        meLikeRV.addOnScrollListener(new MyScrollListener());
        refreshListener.onRefresh();
    }

    public void hideProgressBar() {
        meLikeSRL.setRefreshing(false);
    }

    public void showProgressBar() {
        meLikeSRL.setRefreshing(true);
    }

    public void hideNullData() {
        likeNullLL.setVisibility(View.GONE);
    }

    public void showNullData() {
        likeNullLL.setVisibility(View.VISIBLE);
    }

    private class MyScrollListener extends RecyclerView.OnScrollListener {

        private boolean userScrolled = false;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                userScrolled = true;
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

            // подгрузка
            if (userScrolled && (visibleItemCount + pastVisiblesItems + 3) >= totalItemCount && dy > 0) {
                userScrolled = false;
                presenter.getMorePost();
            }
        }
    }
}
