package ru.bigtail.ui.main.fragments.find;

import android.content.Intent;
import android.view.View;

import ru.bigtail.model.common.Types;
import ru.bigtail.api.callback.SearchCallback;
import ru.bigtail.model.fields.DataFields;
import ru.bigtail.ui.viewers.friends.FriendListActivity;
import ru.bigtail.ui.viewers.group.GroupListActivity;
import ru.bigtail.view.forms.FormsHelper;

/**
 * Created by Павел on 29.07.2017.
 */

public class FindPresenter {

    private FindFragment view;
    private FormsHelper formsHelper;
    private int whereSearch = -1;
    private DataFields saveFields;

    public FindPresenter() {
    }

    public void attachView(FindFragment view) {
        this.view = view;
        formsHelper = new FormsHelper(this.view.getActivity().getLayoutInflater(), this.view.includeView, saveFields);

        formsHelper.getFieldsFriendsSearchForm().getListFriendsBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FindPresenter.this.view.getActivity(), FriendListActivity.class);
                FindPresenter.this.view.getActivity().startActivityForResult(intent, Types.TYPE_FRIENDS);
            }
        });

        formsHelper.getFieldsGroupSearchForm().getListGroupBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FindPresenter.this.view.getActivity(), GroupListActivity.class);
                FindPresenter.this.view.getActivity().startActivityForResult(intent, Types.TYPE_GROUP);
            }
        });

        if (saveFields != null) {
            switch (saveFields.getTypeFields()) {
                case Types.TYPE_FIELDS_FRIENDS:
                    this.view.searchFriend();
                    break;
                case  Types.TYPE_FIELDS_GLOBAL:
                    this.view.searchGlobal();
                    break;
                case Types.TYPE_FIELDS_GROUP:
                    this.view.searchGroup();
                    break;
            }
        }

    }

    public void detachView() {
        this.view = null;
    }

    public boolean searchGroup() {
        if (whereSearch == 0)
            return false;
        whereSearch = 0;
        formsHelper.setSearchGroupFormLayout();
        return true;
    }

    public boolean searchFriend() {
        if (whereSearch == 1)
            return false;
        whereSearch = 1;
        formsHelper.setSearchFriendFormLayout();
        return true;
    }

    public boolean searchGlobal() {
        if (whereSearch == 2)
            return false;
        whereSearch = 2;
        formsHelper.setSearchGlobalFormLayout();
        return true;
    }

    public void getSearchFields(SearchCallback callback) {
        formsHelper.getDataFields(callback);
    }

    public void setSavingFields(DataFields savingFields) {
        this.saveFields = savingFields;
    }
}
