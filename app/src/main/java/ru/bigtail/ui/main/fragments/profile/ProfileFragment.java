package ru.bigtail.ui.main.fragments.profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ru.bigtail.R;
import ru.bigtail.api.vk.VkApiHelper;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.model.common.Types;
import ru.bigtail.ui.intro.IntroActivity;
import ru.bigtail.ui.viewers.friendslike.FriendsLikeActivity;
import ru.bigtail.ui.viewers.mylike.MyLikeActivity;

public class ProfileFragment extends Fragment {

    @BindView(R.id.photoCIV)
    CircleImageView photoCIV;

    @BindView(R.id.nameTV)
    TextView nameTV;

    @BindView(R.id.countTotalLikeTV)
    TextView countTotalLikeTV;

    @BindView(R.id.countDetailLikeTV)
    TextView countDetailLikeTV;

    @BindView(R.id.myLikeTV)
    TextView myLikeTV;

    @BindView(R.id.likeFriendsTV)
    TextView likeFriendsTV;

    @BindView(R.id.ratingTV)
    TextView ratingTV;

    @BindView(R.id.exitTV)
    TextView exitTV;

    @BindView(R.id.aboutTV)
    TextView aboutTV;

    @BindView(R.id.exitLL)
    LinearLayout exitLL;

    @BindView(R.id.ratingLL)
    LinearLayout ratingLL;

    @BindView(R.id.myLikeLL)
    LinearLayout myLikeLL;

    @BindView(R.id.profileSRL)
    SwipeRefreshLayout profileSRL;

    @BindView(R.id.aboutLL)
    LinearLayout aboutLL;

    private ProfilePresenter presenter;
    private SwipeRefreshLayout.OnRefreshListener refreshListener;

    public ProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, rootView);

        presenter = new ProfilePresenter();
        presenter.attachView(this);

        initUI();

        return rootView;
    }

    private void initUI() {
        nameTV.setTypeface(AssetsManager.openSansRegular);
        countTotalLikeTV.setTypeface(AssetsManager.openSansRegular);
        countDetailLikeTV.setTypeface(AssetsManager.openSansRegular);
        myLikeTV.setTypeface(AssetsManager.openSansRegular);
        likeFriendsTV.setTypeface(AssetsManager.openSansRegular);
        ratingTV.setTypeface(AssetsManager.openSansRegular);
        exitTV.setTypeface(AssetsManager.openSansRegular);
        aboutTV.setTypeface(AssetsManager.openSansRegular);

        refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.loadProfile();
            }
        };

        profileSRL.setOnRefreshListener(refreshListener);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                switch (v.getId()) {
                    case R.id.aboutLL:
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.baseBigtailURL)));
                        getActivity().startActivity(intent);
                        break;
                    case R.id.exitLL:
                        VkApiHelper.logout();
                        intent = new Intent(getActivity(), IntroActivity.class);
                        getActivity().startActivity(intent);
                        break;
                    case R.id.ratingLL:
                        showDialogRating();
                        break;
                    case R.id.myLikeLL:
                        intent = new Intent(getActivity(), MyLikeActivity.class);
                        getActivity().startActivityForResult(intent, Types.TYPE_FRIENDS);
                        break;
                    case R.id.likeFriendsTV:
                        intent = new Intent(getActivity(), FriendsLikeActivity.class);
                        getActivity().startActivity(intent);
                        break;
                }
            }
        };

        aboutLL.setOnClickListener(clickListener);
        likeFriendsTV.setOnClickListener(clickListener);
        myLikeLL.setOnClickListener(clickListener);
        exitLL.setOnClickListener(clickListener);
        ratingLL.setOnClickListener(clickListener);

        refreshListener.onRefresh();
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    public void hideProgressBar() {
        profileSRL.setRefreshing(false);
    }

    public void showProgressBar() {
        profileSRL.setRefreshing(true);
    }

    private void showDialogRating() {
        AlertDialog.Builder ad = new AlertDialog.Builder(getActivity());
        ad.setTitle(R.string.rating_title);  // заголовок
        ad.setMessage(R.string.rating_message); // сообщение

        ad.setPositiveButton(R.string.understand, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();
            }
        });

        AlertDialog dialog = ad.show();

        TextView messageTV = (TextView) dialog.findViewById(android.R.id.message);
        if (messageTV != null)
            messageTV.setTypeface(AssetsManager.openSansRegular);

        TextView titleTV = (TextView) dialog.findViewById(R.id.alertTitle);
        if (titleTV != null)
            titleTV.setTypeface(AssetsManager.openSansLight);

        Button button1 = (Button) dialog.findViewById(android.R.id.button1);
        if (button1 != null)
            button1.setTypeface(AssetsManager.openSansRegular);

        Button button2 = (Button) dialog.findViewById(android.R.id.button2);
        if (button2 != null)
            button2.setTypeface(AssetsManager.openSansRegular);
    }

    @Override
    public void onStart() {
        super.onStart();

        getActivity().setTitle(R.string.profile);
    }
}
