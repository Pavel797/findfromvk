package ru.bigtail.ui.main.fragments.find;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.api.callback.SearchCallback;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.model.fields.DataFields;
import ru.bigtail.ui.main.activity.MainActivity;
import ru.bigtail.R;

public class FindFragment extends Fragment {

    @BindView(R.id.includeView)
    LinearLayout includeView;

    @BindView(R.id.searchLL)
    LinearLayout searchLL;

    @BindView(R.id.groupBtn)
    Button groupBtn;

    @BindView(R.id.friendBtn)
    Button friendBtn;

    @BindView(R.id.globalBtn)
    Button globalBtn;

    @BindView(R.id.recomendsTV)
    TextView recomendsTV;

    @BindView(R.id.searchHintTV)
    TextView searchHintTV;

    @BindView(R.id.searchBtnTV)
    TextView searchBtnTV;

    private FindPresenter presenter;
    private DataFields savingFields;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_find, container, false);
        ButterKnife.bind(this, rootView);

        presenter = new FindPresenter();
        presenter.setSavingFields(savingFields);
        presenter.attachView(this);

        initUI();

        if (savingFields == null) {
            presenter.searchGroup();
            groupBtn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.radio_button_style));
        }
        return rootView;
    }

    private void initUI() {
        recomendsTV.setTypeface(AssetsManager.openSansRegular);
        groupBtn.setTypeface(AssetsManager.openSansRegular);
        friendBtn.setTypeface(AssetsManager.openSansRegular);
        globalBtn.setTypeface(AssetsManager.openSansRegular);
        searchHintTV.setTypeface(AssetsManager.openSansRegular);
        searchBtnTV.setTypeface(AssetsManager.openSansRegular);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.searchLL:
                        presenter.getSearchFields(new SearchCallback() {
                            @Override
                            public void search(DataFields fields) {
                                MainActivity activity = (MainActivity) getActivity();
                                activity.search(fields);
                            }
                        });
                        break;
                    case R.id.recomendsTV:
                        //Toast.makeText(getActivity(), "Тут ебать рекомендашки", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.groupBtn:
                        searchGroup();
                        break;
                    case R.id.friendBtn:
                        searchFriend();
                        break;
                    case R.id.globalBtn:
                        searchGlobal();
                        break;
                }
            }
        };

        recomendsTV.setOnClickListener(onClickListener);
        globalBtn.setOnClickListener(onClickListener);
        friendBtn.setOnClickListener(onClickListener);
        groupBtn.setOnClickListener(onClickListener);
        searchLL.setOnClickListener(onClickListener);
    }

    public void searchGlobal() {
        if (presenter.searchGlobal()) {
            globalBtn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.radio_button_style));
            groupBtn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.search_button_style));
            friendBtn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.search_button_style));
        }
    }

    public void searchFriend() {
        if (presenter.searchFriend()) {
            friendBtn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.radio_button_style));
            groupBtn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.search_button_style));
            globalBtn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.search_button_style));
        }
    }

    public void searchGroup() {
        if (presenter.searchGroup()) {
            groupBtn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.radio_button_style));
            friendBtn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.search_button_style));
            globalBtn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.search_button_style));
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        getActivity().setTitle(R.string.find_title);

        View thisView = getView();
        if (thisView != null)
            thisView.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fadein));
    }

    public void setSavingFields(DataFields savingFields) {
        this.savingFields = savingFields;
    }
}
