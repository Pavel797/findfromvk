package ru.bigtail.ui.main.fragments.profile;

import android.support.v7.widget.AppCompatDrawableManager;
import android.text.Html;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.bigtail.R;
import ru.bigtail.api.app.BigtailApiManager;
import ru.bigtail.api.app.BigtailErrorHelper;
import ru.bigtail.model.profile.ProfileModelAnswer;
import ru.bigtail.util.TextFormatter;

/**
 * Created by Павел on 21.08.2017.
 */

public class ProfilePresenter {

    private ProfileFragment view;
    private Call<ProfileModelAnswer> callGetProfile;

    public ProfilePresenter() {
    }

    public void attachView(ProfileFragment view) {
        this.view = view;
    }

    public void loadProfile() {
        view.showProgressBar();
        callGetProfile = BigtailApiManager.getProfile(new Callback<ProfileModelAnswer>() {
            @Override
            public void onResponse(Call<ProfileModelAnswer> call, Response<ProfileModelAnswer> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ProfileModelAnswer answer = response.body();
                    if (BigtailErrorHelper.error(answer.getErrorCode(), answer.getErrorMessage()) && !call.isCanceled()) {
                        setData(response.body());
                    }
                }
                view.hideProgressBar();
            }

            @Override
            public void onFailure(Call<ProfileModelAnswer> call, Throwable t) {
                if (!call.isCanceled()) {
                    view.hideProgressBar();
                    BigtailErrorHelper.onFailure(t);
                }
            }
        });
    }

    public void setData(ProfileModelAnswer profile) {
        Picasso.with(view.photoCIV.getContext())
                .load(profile.getResponse().getPhoto100())
                .memoryPolicy(MemoryPolicy.NO_STORE, MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                .fit()
                //.centerInside()
                .placeholder(AppCompatDrawableManager.get().getDrawable(view.photoCIV.getContext(), R.drawable.ic_more_horiz))
                .error(AppCompatDrawableManager.get().getDrawable(view.photoCIV.getContext(), R.drawable.ic_error_outline))
                .into(view.photoCIV);

        String countTotalLikeText = "Вас оценили <font color='#F1796D'>" + TextFormatter.getFormatCounterLikes(profile.getResponse().getLikesCountTotal()) + "</font> раз(а), из них:";
        view.countTotalLikeTV.setText(Html.fromHtml(countTotalLikeText));

        String countDetailLikeText = "парни: <font color='#8495AE'>" + TextFormatter.getFormatCounterLikes(profile.getResponse().getLikesCountFromMan()) + "</font>," +
                " девушки: <font color='#F1796D'>" + TextFormatter.getFormatCounterLikes(profile.getResponse().getLikesCountFromWomen()) + "</font> раз(а)";

        view.countTotalLikeTV.setText(Html.fromHtml(countTotalLikeText));
        view.countDetailLikeTV.setText(Html.fromHtml(countDetailLikeText));

        view.nameTV.setText(profile.getResponse().getFirstName() + " " + profile.getResponse().getLastName());
    }

    public void detachView() {
        if (callGetProfile != null && !callGetProfile.isCanceled() && !callGetProfile.isExecuted())
            callGetProfile.cancel();
    }
}
