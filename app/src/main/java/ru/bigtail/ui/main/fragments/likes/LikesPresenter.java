package ru.bigtail.ui.main.fragments.likes;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.bigtail.api.app.BigtailApiManager;
import ru.bigtail.api.app.BigtailErrorHelper;
import ru.bigtail.api.vk.VkApiHelper;
import ru.bigtail.model.answer.ModelAnswerUser;
import ru.bigtail.ui.adapter.list.UserListAdapter;
import ru.bigtail.ui.main.activity.MainActivity;

/**
 * Created by Павел on 07.08.2017.
 */

public class LikesPresenter {

    private LikesFragment view;
    private UserListAdapter adapter;
    private Call<ModelAnswerUser> callWhoLikeMe;

    public void attachView(LikesFragment likesFragment) {
        this.view = likesFragment;
        adapter = new UserListAdapter((MainActivity) view.getActivity());
    }


    public UserListAdapter getAdapter() {
        return adapter;
    }

    public void onRefresh() {
        view.hideNullData();
        view.showProgressBar();
        adapter.clear();
        adapter.setEnd(false);
        loadResults(0);
    }

    public void getMorePost() {
        loadResults(adapter.getItemCount());
    }

    public void loadResults(long offset) {
        callWhoLikeMe = BigtailApiManager.whoLikeMe(offset, new Callback<ModelAnswerUser>() {
            @Override
            public void onResponse(Call<ModelAnswerUser> call, Response<ModelAnswerUser> response) {
                ModelAnswerUser answer = response.body();
                if (response.isSuccessful() && answer != null &&
                        BigtailErrorHelper.error(answer.getErrorCode(), answer.getErrorMessage())) {
                    setDataInAdapter(response.body());
                }

                view.hideProgressBar();
            }

            @Override
            public void onFailure(Call<ModelAnswerUser> call, Throwable t) {
                if (!call.isCanceled()) {
                    view.hideProgressBar();
                    BigtailErrorHelper.onFailure(t);
                }
            }
        });
    }

    public void setDataInAdapter(ModelAnswerUser modelAnswerUser) {
        if (modelAnswerUser.getResponse().getItems().size() < VkApiHelper.count_get)
            adapter.setEnd(true);
        adapter.addUserCard(modelAnswerUser.getResponse().getItems());
        view.hideProgressBar();

        if (adapter.getOffset() == 0) {
            view.showNullData();
        }
    }

    public void detachView() {
        adapter.detachView();
        if (callWhoLikeMe != null)
            callWhoLikeMe.cancel();
    }
}
