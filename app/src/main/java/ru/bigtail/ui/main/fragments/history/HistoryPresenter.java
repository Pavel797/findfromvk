package ru.bigtail.ui.main.fragments.history;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.bigtail.api.app.BigtailErrorHelper;
import ru.bigtail.api.vk.VkApiHelper;
import ru.bigtail.model.history.ModelAnswerHistory;
import ru.bigtail.ui.adapter.history.HistoryAdapter;
import ru.bigtail.api.app.BigtailApiManager;

/**
 * Created by Павел on 13.08.2017.
 */

public class HistoryPresenter {

    private HistoryFragment view;
    private HistoryAdapter adapter;
    private Call<ModelAnswerHistory> callHistoryGetList;

    public HistoryPresenter() {
    }

    public void attachView(HistoryFragment historyFragment) {
        this.view = historyFragment;
        adapter = new HistoryAdapter(view.getActivity());
    }

    public void onRefresh() {
        view.nullData(false);
        view.refreshHistorySRL.setRefreshing(true);
        adapter.setEnd(false);
        adapter.clear();
        loadResults(0);
    }

    public void loadResults(long offset) {
        callHistoryGetList = BigtailApiManager.historyGetList(offset, new Callback<ModelAnswerHistory>() {
            @Override
            public void onResponse(Call<ModelAnswerHistory> call, Response<ModelAnswerHistory> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ModelAnswerHistory answer = response.body();

                    if (BigtailErrorHelper.error(answer.getErrorCode(), answer.getErrorMessage())) {
                        if (answer.getResponse().size() < VkApiHelper.count_get) {
                            adapter.setEnd(true);
                        }
                    }

                    adapter.addHistory(response.body().getResponse());
                    if (adapter.getOffset() == 0)
                        view.nullData(true);
                } else {
                    BigtailErrorHelper.requestNotSuccessful(response.code(), response.message());
                }
                view.refreshHistorySRL.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ModelAnswerHistory> call, Throwable t) {
                if (!call.isCanceled()) {
                    view.refreshHistorySRL.setRefreshing(false);
                    BigtailErrorHelper.onFailure(t);
                }
            }
        });
    }

    public void getMorePost() {
        loadResults(adapter.getOffset());
    }

    public HistoryAdapter getAdapter() {
        return adapter;
    }

    public void detachView() {
        adapter.detachView();
        if (callHistoryGetList != null)
            callHistoryGetList.cancel();
    }
}
