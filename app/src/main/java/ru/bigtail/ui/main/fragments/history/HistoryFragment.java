package ru.bigtail.ui.main.fragments.history;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.R;
import ru.bigtail.model.assets.AssetsManager;

public class HistoryFragment extends Fragment {

    @BindView(R.id.refreshHistorySRL)
    SwipeRefreshLayout refreshHistorySRL;

    @BindView(R.id.historyRV)
    RecyclerView historyRV;

    @BindView(R.id.historyNullTV)
    TextView historyNullTV;

    @BindView(R.id.historyNullLL)
    LinearLayout historyNullLL;

    private HistoryPresenter presenter;
    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout.OnRefreshListener refreshListener;

    public HistoryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, rootView);

        presenter = new HistoryPresenter();
        presenter.attachView(this);

        setIU();

        return rootView;
    }

    private void setIU() {
        historyNullTV.setTypeface(AssetsManager.openSansRegular);

        refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.onRefresh();
            }
        };

        refreshHistorySRL.setOnRefreshListener(refreshListener);
        layoutManager = new LinearLayoutManager(getActivity());
        historyRV.setLayoutManager(layoutManager);
        historyRV.setAdapter(presenter.getAdapter());
        historyRV.addOnScrollListener(new MyScrollListener());
        refreshListener.onRefresh();
    }

    @Override
    public void onDetach() {
        presenter.detachView();
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();

        getActivity().setTitle(R.string.history_title);
    }

    public void nullData(boolean val) {
        if (val) {
            historyNullLL.setVisibility(View.VISIBLE);
        } else {
            historyNullLL.setVisibility(View.GONE);
        }
    }

    private class MyScrollListener extends RecyclerView.OnScrollListener {

        private boolean userScrolled = false;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                userScrolled = true;
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

            // подгрузка
            if (userScrolled && (visibleItemCount + pastVisiblesItems + 3) >= totalItemCount && dy > 0) {
                userScrolled = false;
                presenter.getMorePost();
            }
        }
    }
}
