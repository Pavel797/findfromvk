package ru.bigtail.ui.main.fragments.results.card;

import android.app.Activity;

import ru.bigtail.model.answer.ModelAnswerUser;
import ru.bigtail.api.callback.GetMoreCallback;
import ru.bigtail.api.callback.SetResultCallback;
import ru.bigtail.ui.adapter.card.card.ContentCardAdapter;
import ru.bigtail.ui.main.fragments.results.ResultsPresenter;

/**
 * Created by Павел on 06.08.2017.
 */

public class CardPresenter {

    private CardLayout view;
    private ContentCardAdapter adapter;
    private ResultsPresenter resultsPresenter;

    public CardPresenter(ResultsPresenter resultsPresenter) {
        this.resultsPresenter = resultsPresenter;
    }

    public void attachView(CardLayout cardLayout, Activity activity) {
        this.view = cardLayout;

        adapter = new ContentCardAdapter(activity);
        adapter.setLoadMoreCallback(new GetMoreCallback() {
            @Override
            public void getMore() {
                view.showProgressBar();
                loadResults();
            }
        });
    }

    public void loadResults() {
        resultsPresenter.loadResults(new SetResultCallback() {
            @Override
            public void setData(ModelAnswerUser data) {
                setDataInAdapter(data);
            }

            @Override
            public void error() {
                view.showError();
                view.tryAgainACIV.clearAnimation();
                view.tryAgainACIV.setClickable(true);
            }

            @Override
            public void hideProgressBar() {
                view.tryAgainACIV.clearAnimation();
                view.tryAgainACIV.setClickable(true);
                view.hideProgressBar();
            }

            @Override
            public void end() {

            }

            @Override
            public int getUsersCount() {
                return adapter.getSize();
            }
        });
    }

    public void setDataInAdapter(ModelAnswerUser modelAnswerUser) {
        if (adapter.getCount() == 0)
            view.sliderVP.setAdapter(adapter);
        if (modelAnswerUser.getResponse().getItems().size() != 0)
            adapter.addUserCard(modelAnswerUser.getResponse().getItems());
        view.hideProgressBar();
    }

    public void detachView() {
        // TODO: 24.08.2017 хз как обрабатовать ошибки сети на кардах
    }
}
