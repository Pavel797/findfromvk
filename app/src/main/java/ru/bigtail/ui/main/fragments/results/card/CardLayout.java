package ru.bigtail.ui.main.fragments.results.card;

import android.app.Activity;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ToxicBakery.viewpager.transforms.TabletTransformer;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.R;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.ui.main.fragments.results.ResultsPresenter;

public class CardLayout {

    @BindView(R.id.sliderVP)
    ViewPager sliderVP;

    @BindView(R.id.progressBarLL)
    LinearLayout progressBarLL;

    @BindView(R.id.loadTV)
    TextView loadTV;

    @BindView(R.id.tryAgainACIV)
    AppCompatImageView tryAgainACIV;

    private CardPresenter presenter;
    private View rootView;

    public CardLayout(Activity activity, LinearLayout linearLayout, ResultsPresenter resultsPresenter) {
        presenter = new CardPresenter(resultsPresenter);
        rootView = activity.getLayoutInflater().inflate(R.layout.fragment_card, linearLayout, true);
        ButterKnife.bind(this, rootView);
        presenter.attachView(this, activity);

        initUI();

    }

    private void initUI() {
        loadTV.setTypeface(AssetsManager.openSansRegular);

        sliderVP.setPageTransformer(true, new TabletTransformer());
        sliderVP.setVisibility(View.INVISIBLE);

        showProgressBar();

        tryAgainACIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryAgainACIV.setClickable(false);
                tryAgainACIV.startAnimation(AnimationUtils.loadAnimation(tryAgainACIV.getContext(), R.anim.rotate_center));
                presenter.loadResults();
            }
        });

        presenter.loadResults();
    }

    public void showProgressBar() {
        tryAgainACIV.clearAnimation();
        tryAgainACIV.setClickable(true);
        tryAgainACIV.setVisibility(View.GONE);
        loadTV.setText(rootView.getResources().getString(R.string.load));
        YoYo.with(Techniques.Flash)
                .duration(1200)
                .playOn(loadTV);
        progressBarLL.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        tryAgainACIV.clearAnimation();
        tryAgainACIV.setClickable(true);
        tryAgainACIV.setVisibility(View.GONE);
        loadTV.setText(rootView.getResources().getString(R.string.load));
        sliderVP.setVisibility(View.VISIBLE);
        progressBarLL.setVisibility(View.GONE);
    }

    public View getView() {
        return rootView;
    }

    public void showError() {
        loadTV.setText(rootView.getResources().getString(R.string.error));
        tryAgainACIV.setVisibility(View.VISIBLE);
    }

    public void detachView() {
        presenter.detachView();
    }
}
