package ru.bigtail.ui.main.fragments.results.list;


import android.app.Activity;

import ru.bigtail.api.callback.SetResultCallback;
import ru.bigtail.api.vk.VkApiHelper;
import ru.bigtail.model.answer.ModelAnswerUser;
import ru.bigtail.ui.adapter.list.UserListAdapter;
import ru.bigtail.ui.main.activity.MainActivity;
import ru.bigtail.ui.main.fragments.results.ResultsPresenter;

/**
 * Created by Павел on 06.08.2017.
 */

public class ListPresenter {

    private ListLayout view;
    private UserListAdapter adapter;
    private ResultsPresenter resultsPresenter;

    public ListPresenter(ResultsPresenter resultsPresenter) {
        this.resultsPresenter = resultsPresenter;
    }

    public void attachView(ListLayout view, Activity activity) {
        this.view = view;
        adapter = new UserListAdapter((MainActivity)activity);
    }

    public void loadResults() {
        resultsPresenter.loadResults(new SetResultCallback() {
            @Override
            public void setData(ModelAnswerUser data) {
                setDataInAdapter(data);
            }

            @Override
            public void error() {
                view.showError();
                view.hideProgressBar();
            }

            @Override
            public void hideProgressBar() {
                view.hideProgressBar();
            }

            @Override
            public void end() {
                adapter.setEnd(true);
            }

            @Override
            public int getUsersCount() {
                return (int)adapter.getOffset();
            }
        });
    }

    public void setDataInAdapter(ModelAnswerUser modelAnswerUser) {
        if (modelAnswerUser.getResponse().getItems().size() < VkApiHelper.count_get)
            adapter.setEnd(true);
        adapter.addUserCard(modelAnswerUser.getResponse().getItems());
        view.hideProgressBar();
    }

    public void onRefresh() {
        view.showProgressBar();
        view.hideError();
        adapter.clear();
        adapter.setEnd(false);
        resultsPresenter.resetOffset();
        loadResults();
    }

    public UserListAdapter getAdapter() {
        return adapter;
    }

    public void getMorePost() {
        loadResults();
    }

    public void detachView() {
        adapter.detachView();
    }
}
