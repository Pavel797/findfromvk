package ru.bigtail.ui.main.activity;

import android.app.Activity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.content.Intent;

import com.vk.sdk.VKSdk;

import ru.bigtail.model.common.Types;
import ru.bigtail.model.fields.DataFields;
import ru.bigtail.model.fields.DataFieldsFriendsSearch;
import ru.bigtail.model.fields.DataFieldsGroupSearch;
import ru.bigtail.model.preferences.PreferencesHelper;
import ru.bigtail.ui.main.fragments.history.HistoryFragment;
import ru.bigtail.ui.main.fragments.profile.ProfileFragment;
import ru.bigtail.R;
import ru.bigtail.ui.intro.IntroActivity;
import ru.bigtail.ui.main.fragments.find.FindFragment;
import ru.bigtail.ui.main.fragments.likes.LikesFragment;
import ru.bigtail.ui.main.fragments.results.ResultsFragment;

/**
 * Created by Павел on 29.07.2017.
 */

public class MainPresenter {

    private MainActivity view;

    //private FindFragment findFragment;
    //private HistoryFragment historyFragment;
    //private ResultsFragment resultsFragment;
    //private LikesFragment likesFragment;
    //private ProfileFragment profileFragment;

    private DataFields saveFields;

    public MainPresenter() {
        //findFragment = new FindFragment();
        //historyFragment = new HistoryFragment();
        //resultsFragment = new ResultsFragment();
        //likesFragment = new LikesFragment();
        //profileFragment = new ProfileFragment();
        saveFields = null;
    }

    public boolean checkLogin() {
        if (PreferencesHelper.isFirstApplicationLaunch() || !VKSdk.isLoggedIn()) {
            Intent intent = new Intent(view, IntroActivity.class);
            view.startActivity(intent);
            return false;
        }
        return true;
    }

    public void attachView(MainActivity view) {
        this.view = view;
    }

    public void detachView() {
        this.view = null;
    }

    public void selectedFind() {
        FindFragment findFragment = new FindFragment();
        findFragment.setSavingFields(saveFields);
        setFragment(findFragment);
    }

    public void selectedHistory() {
        setFragment(new HistoryFragment());
    }

    public void selectedResults() {
        setFragment(new ResultsFragment());
    }

    public void selectedResults(DataFields dataFields) {
        saveFields = dataFields;
        ResultsFragment resultsFragment = new ResultsFragment();
        resultsFragment.setDataFields(dataFields);
        setFragment(resultsFragment);
    }

    public void selectedLikes() {
        setFragment(new LikesFragment());
    }

    public void selectedProfile() {
        setFragment(new ProfileFragment());
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction ftrans = view.getSupportFragmentManager().beginTransaction();
        ftrans.replace(R.id.container, fragment);
        ftrans.commit();
    }

    public void setFields(DataFields fields) {
        this.saveFields = fields;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK){
            FindFragment findFragment;
            switch (requestCode){
                case Types.TYPE_FRIENDS:
                    view.openForm(new DataFieldsFriendsSearch(data.getStringExtra(Types.link), ""));
                    break;
                case Types.TYPE_GROUP:
                    findFragment = new FindFragment();
                    saveFields = new DataFieldsGroupSearch(0, "", data.getStringExtra(Types.link), null, null, null, null);
                    findFragment.setSavingFields(saveFields);
                    setFragment(findFragment);
                    break;
            }
        }
    }
}
