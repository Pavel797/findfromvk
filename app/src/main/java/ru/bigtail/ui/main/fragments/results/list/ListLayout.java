package ru.bigtail.ui.main.fragments.results.list;

import android.app.Activity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.R;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.ui.main.fragments.results.ResultsPresenter;

public class ListLayout {

    @BindView(R.id.refreshResultsSRL)
    SwipeRefreshLayout refreshResultsSRL;

    @BindView(R.id.resultsRV)
    RecyclerView resultsRV;

    @BindView(R.id.ryAgainLL)
    LinearLayout ryAgainLL;

    @BindView(R.id.ryAgainTV)
    TextView ryAgainTV;

    @BindView(R.id.tryAgainACIV)
    AppCompatImageView tryAgainACIV;

    private SwipeRefreshLayout.OnRefreshListener refreshListener;
    private LinearLayoutManager layoutManager;
    private ListPresenter presenter;
    private View rootView;

    public ListLayout(Activity activity, LinearLayout linearLayout, ResultsPresenter resultsPresenter) {
        rootView = activity.getLayoutInflater().inflate(R.layout.fragment_list, linearLayout, true);
        ButterKnife.bind(this, rootView);

        presenter = new ListPresenter(resultsPresenter);
        presenter.attachView(this, activity);

        initUI(activity);
    }

    private void initUI(Activity activity) {
        ryAgainTV.setTypeface(AssetsManager.openSansRegular);

        refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.onRefresh();
            }
        };

        tryAgainACIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.loadResults();
            }
        });

        refreshResultsSRL.setOnRefreshListener(refreshListener);
        layoutManager = new LinearLayoutManager(activity);
        resultsRV.setLayoutManager(layoutManager);
        resultsRV.setAdapter(presenter.getAdapter());
        resultsRV.addOnScrollListener(new MyScrollListener());
        refreshListener.onRefresh();
    }

    public void hideProgressBar() {
        refreshResultsSRL.setRefreshing(false);
    }

    public void showProgressBar() {
        refreshResultsSRL.setRefreshing(true);
    }


    public View getView() {
        return rootView;
    }

    public void showError() {
        ryAgainLL.setVisibility(View.VISIBLE);
    }

    public void hideError() {
        ryAgainLL.setVisibility(View.GONE);
    }

    public void detachView() {
        presenter.detachView();
    }

    private class MyScrollListener extends RecyclerView.OnScrollListener {

        private boolean userScrolled = false;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                userScrolled = true;
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

            // подгрузка
            if (userScrolled && (visibleItemCount + pastVisiblesItems + 3) >= totalItemCount && dy > 0) {
                userScrolled = false;
                presenter.getMorePost();
            }
        }
    }
}
