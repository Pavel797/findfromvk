package ru.bigtail.ui.main.fragments.results;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.Button;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.bigtail.R;
import ru.bigtail.api.app.BigtailApiManager;
import ru.bigtail.api.app.BigtailErrorHelper;
import ru.bigtail.api.callback.SetResultCallback;
import ru.bigtail.api.vk.VkApiHelper;
import ru.bigtail.model.answer.ModelAnswerUser;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.model.common.Types;
import ru.bigtail.model.fields.DataFields;
import ru.bigtail.model.fields.DataFieldsFriendsSearch;
import ru.bigtail.model.fields.DataFieldsGlobalSearch;
import ru.bigtail.model.fields.DataFieldsGroupSearch;
import ru.bigtail.model.history.HistoryObject;
import ru.bigtail.model.history.ModelAnswerHistory;
import ru.bigtail.model.history.ModelHistory;
import ru.bigtail.model.preferences.PreferencesHelper;
import ru.bigtail.ui.main.fragments.results.card.CardLayout;
import ru.bigtail.ui.main.fragments.results.list.ListLayout;

/**
 * Created by Павел on 02.08.2017.
 *
 * Это presenter фрагмента резульатов, тут происходит
 * основная работа с сетью и обработка данных для дальнейшего их отображения
 */

public class ResultsPresenter {

    private ResultsFragment view;
    private DataFields dataFields;
    private Call getHistoryCall, getSearchCall;
    private CardLayout cardLayout;
    private ListLayout listLayout;
    private int isEnd = -1; // -1 - нет конца, 0 - есть конец и грузим последнюю порцию, 1 - последняя порция загружена и конец установлен

    public ResultsPresenter() {
    }

    public void attachView(final ResultsFragment view) {
        this.view = view;
    }

    public void setDataFields(DataFields dataFields) {
        this.dataFields = dataFields;
    }

    public void viewResult() {
        if (PreferencesHelper.isViewInCards()) {
            viewInCards();
        } else {
            viewInList();
        }
    }

    public void loadResults(final SetResultCallback resultCallback) {
        view.setNullData(false);

        if (isEnd == 1) {
            resultCallback.hideProgressBar();
            resultCallback.end();
            return;
        }
        else if (isEnd == 0)
            isEnd = 1;

        if (dataFields == null) {
            getHistory(resultCallback);
            return;
        }
        getSearchCall = BigtailApiManager.search(dataFields, new Callback<ModelAnswerUser>() {
            @Override
            public void onResponse(Call<ModelAnswerUser> call, Response<ModelAnswerUser> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ModelAnswerUser answer = response.body();
                    if (BigtailErrorHelper.error(answer.getErrorCode(), answer.getErrorMessage())) {
                        if (answer.getResponse().isHistoryExist()) {
                            showDialog(answer, resultCallback);
                        } else {
                            if (answer.getResponse().getItems().size() == 0 && dataFields.getOffset() != 0) {
                                dataFields.setOffset(dataFields.getOffset() - (int)VkApiHelper.count_get);
                                if (resultCallback.getUsersCount() == 0) {
                                    isEnd = 0;
                                    resultCallback.end();
                                    loadResults(resultCallback);
                                } else {
                                    isEnd = 1;
                                    resultCallback.end();
                                    resultCallback.hideProgressBar();
                                }
                                // TODO: 25.08.2017 testing !!!
                            } else {
                                dataFields.setOffset(dataFields.getOffset() + answer.getResponse().getItems().size());
                                resultCallback.setData(answer);
                                dataFields.setHistoryId(answer.getResponse().getHistoryId());
                            }
                        }
                    }
                } else {
                    resultCallback.error();
                    BigtailErrorHelper.requestNotSuccessful(response.code(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ModelAnswerUser> call, Throwable t) {
                if (!call.isCanceled()) {
                    resultCallback.error();
                    BigtailErrorHelper.onFailure(t);
                }
            }
        });
    }

    private void showDialog(final ModelAnswerUser modelAnswerUser, final SetResultCallback resultCallback) {
        AlertDialog.Builder ad = new AlertDialog.Builder(view.getActivity());
        ad.setTitle(R.string.title_dialog_research);  // заголовок
        ad.setMessage(R.string.message_dialog_research); // сообщение

        ad.setPositiveButton("Начать заново", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dataFields.setIgnoreHistory(true);
                dataFields.setOffset(0);
                loadResults(resultCallback);
            }
        });
        ad.setNegativeButton("Продолжить", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dataFields.setHistoryId(modelAnswerUser.getResponse().getHistoryId());
                dataFields.setOffset(modelAnswerUser.getResponse().getOffset());
                loadResults(resultCallback);
            }
        });
        ad.setCancelable(false);
        ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                showDialog(modelAnswerUser, resultCallback);
            }
        });

        AlertDialog dialog = ad.show();

        TextView messageTV = (TextView) dialog.findViewById(android.R.id.message);
        if (messageTV != null)
            messageTV.setTypeface(AssetsManager.openSansRegular);

        TextView titleTV = (TextView) dialog.findViewById(R.id.alertTitle);
        if (titleTV != null)
            titleTV.setTypeface(AssetsManager.openSansLight);

        Button button1 = (Button) dialog.findViewById(android.R.id.button1);
        if (button1 != null)
            button1.setTypeface(AssetsManager.openSansRegular);

        Button button2 = (Button) dialog.findViewById(android.R.id.button2);
        if (button2 != null)
            button2.setTypeface(AssetsManager.openSansRegular);
    }

    public void viewInCards() {
        if (listLayout != null)
            listLayout.detachView();
        view.container.removeAllViews();
        cardLayout = new CardLayout(view.getActivity(), view.container, this);
    }

    public void viewInList() {
        if (cardLayout != null)
            cardLayout.detachView();
        view.container.removeAllViews();
        listLayout = new ListLayout(view.getActivity(), view.container, this);
    }

    public void resetOffset() {
        if (dataFields == null)
            return;
        if (dataFields.getOffset() >= VkApiHelper.count_get)
            dataFields.setOffset(dataFields.getOffset() - (int) VkApiHelper.count_get);
        else
            dataFields.setOffset(0);
    }

    public void getHistory(final SetResultCallback resultCallback) {
        getHistoryCall = BigtailApiManager.historyGetList(0L, new Callback<ModelAnswerHistory>() {
            @Override
            public void onResponse(Call<ModelAnswerHistory> call, Response<ModelAnswerHistory> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ModelAnswerHistory answer = response.body();
                    if (response.isSuccessful()
                            && BigtailErrorHelper.error(answer.getErrorCode(), answer.getErrorMessage())
                            && answer.getResponse() != null && !call.isCanceled()) {
                        if (answer.getResponse().size() > 0) {
                            ModelHistory history = answer.getResponse().get(0);
                            reSearch(history.getType(), history.getHistoryObject(),
                                    history.getOffset(), history.getHistoryId());

                            loadResults(resultCallback);
                        } else {
                            resultCallback.hideProgressBar();
                            view.setNullData(true);
                        }
                    } else {
                        resultCallback.error();
                        BigtailErrorHelper.requestNotSuccessful(response.code(), response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelAnswerHistory> call, Throwable t) {
                if (!call.isCanceled()) {
                    resultCallback.error();
                    BigtailErrorHelper.onFailure(t);
                }
            }
        });
    }

    private void reSearch(String type, HistoryObject historyObject, int offset, String historyId) {
        DataFields dataFields = getFields(type, historyObject);
        dataFields.setOffset(offset);
        dataFields.setHistoryId(historyId);
        this.dataFields = dataFields;
    }

    private DataFields getFields(String type, HistoryObject historyObject) {
        DataFields resFields = null;

        switch (type) {
            case Types.Type_global:
                resFields = new DataFieldsGlobalSearch(historyObject.getSex(),
                        historyObject.getQ(), historyObject.getAge_from(), historyObject.getAge_to(),
                        historyObject.getCity(), historyObject.getCountry());
                break;
            case Types.Type_group:
                resFields = new DataFieldsGroupSearch(historyObject.getSex(),
                        historyObject.getQ(), String.valueOf(view.getString(R.string.baseVkURL) + historyObject.getScreenName()),
                        historyObject.getAge_from(), historyObject.getAge_to(),
                        historyObject.getCity(), historyObject.getCountry());
                break;
            case Types.Type_friend:
                resFields = new DataFieldsFriendsSearch(
                        String.valueOf(view.getString(R.string.baseVkURL) + historyObject.getScreenName()),
                        historyObject.getQ());
                break;
        }
        return resFields;
    }

    public void detachView() {
        if (getHistoryCall != null)
            getHistoryCall.cancel();

        if (getSearchCall != null)
            getSearchCall.cancel();

        if (cardLayout != null)
            cardLayout.detachView();

        if (listLayout != null)
            listLayout.detachView();
    }
}
