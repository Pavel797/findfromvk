package ru.bigtail.ui.main.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.R;
import ru.bigtail.model.activity.SearchActivity;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.model.fields.DataFields;

public class MainActivity extends AppCompatActivity implements SearchActivity {

    @BindView(R.id.mainTB)
    Toolbar mainTB;

    @BindView(R.id.mainBNV)
    BottomNavigationViewEx mainBNV;

    @BindView(R.id.titleTV)
    TextView titleTV;

    private MainPresenter presenter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mainTB);

        presenter = new MainPresenter();
        presenter.attachView(this);

        setUI();
    }

    private void setUI() {
        titleTV.setTypeface(AssetsManager.openSansLight);

        //mainBNV.enableAnimation(false);
        mainBNV.enableShiftingMode(false);
        //mainBNV.enableItemShiftingMode(false);
        mainBNV.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.search_item:
                        if (!mainBNV.getMenu().getItem(0).isChecked())
                            presenter.selectedFind();
                        break;
                    case R.id.history_item:
                        if (!mainBNV.getMenu().getItem(1).isChecked())
                            presenter.selectedHistory();
                        break;
                    case R.id.results_item:
                        if (!mainBNV.getMenu().getItem(2).isChecked())
                            presenter.selectedResults();
                        break;
                    case R.id.likes_item:
                        if (!mainBNV.getMenu().getItem(3).isChecked())
                            presenter.selectedLikes();
                        break;
                    case R.id.profile_item:
                        if (!mainBNV.getMenu().getItem(4).isChecked())
                            presenter.selectedProfile();
                        break;
                }
                return true;
            }
        });


        presenter.selectedFind();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void setTitle(int titleId) {
        super.setTitle(titleId);
        titleTV.setText(titleId);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // проверяем авторизацию
        presenter.checkLogin();
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }
    public void search(DataFields searchFields) {
        mainBNV.getMenu().getItem(2).setChecked(true);
        presenter.selectedResults(searchFields);
    }

    public void openForm(DataFields fields) {
        presenter.setFields(fields);
        mainBNV.getMenu().getItem(0).setChecked(true);
        presenter.selectedFind();
    }
}
