package ru.bigtail.ui.viewers.friends;

import android.support.annotation.NonNull;

import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKUsersArray;

import ru.bigtail.api.vk.VkApiHelper;
import ru.bigtail.vedroidMVP.base.presenter.BasePresenter;

public class FriendListPresenter extends BasePresenter<VKUsersArray, FriendListActivity>{

    public void loadData(long offset) {
        startLoadingData();

        VkApiHelper.getMyFriends(offset, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                setModel((VKUsersArray) response.parsedModel);

                finishLoadingData();
            }

            @Override
            public void onError(VKError error) {
                finishLoadingData();
                super.onError(error);
            }
        });
    }


    @Override
    public void bindView(@NonNull FriendListActivity view) {
        super.bindView(view);

        if (model == null && !isLoadingData()) {
            loadData(0);
        }
    }

    @Override
    protected void updateView() {
        view().addFriend(model);
    }


}
