package ru.bigtail.ui.viewers.group;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.AbsListView;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.R;

public class GroupListActivity extends AppCompatActivity {

    @BindView(R.id.loadGroupPB)
    ProgressBar loadGroupPB;

    @BindView(R.id.groupRV)
    RecyclerView groupRV;

    private LinearLayoutManager layoutManager;
    private GroupListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_list);
        ButterKnife.bind(this);
        presenter = new GroupListPresenter();
        presenter.attachView(this);

        initUI();
    }

    private void initUI() {
        layoutManager = new LinearLayoutManager(this);
        groupRV.setLayoutManager(layoutManager);
        groupRV.setAdapter(presenter.getAdapter());
        groupRV.addOnScrollListener(new MyScrollListener());

        presenter.load();
    }

    private class MyScrollListener extends RecyclerView.OnScrollListener {

        private boolean userScrolled = false;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                userScrolled = true;
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

            // подгрузка
            if (userScrolled && (visibleItemCount + pastVisiblesItems + 3) >= totalItemCount && dy > 0) {
                userScrolled = false;
                presenter.getMorePost();
            }
        }
    }
}
