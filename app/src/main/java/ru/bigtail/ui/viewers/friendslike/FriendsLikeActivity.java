package ru.bigtail.ui.viewers.friendslike;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.R;
import ru.bigtail.model.assets.AssetsManager;

public class FriendsLikeActivity extends AppCompatActivity {

    @BindView(R.id.titleTV)
    TextView titleTV;

    @BindView(R.id.loadFriendPB)
    ProgressBar loadFriendPB;

    @BindView(R.id.friendRV)
    RecyclerView friendRV;

    private LinearLayoutManager layoutManager;
    private FriendsLikePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_like);
        ButterKnife.bind(this);

        presenter = new FriendsLikePresenter();
        presenter.attachView(this);

        setUI();
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    private void setUI() {
        titleTV.setTypeface(AssetsManager.openSansLight);

        layoutManager = new LinearLayoutManager(this);
        friendRV.setLayoutManager(layoutManager);
        friendRV.setAdapter(presenter.getAdapter());
        friendRV.addOnScrollListener(new MyScrollListener());

        presenter.load();
    }

    private class MyScrollListener extends RecyclerView.OnScrollListener {

        private boolean userScrolled = false;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                userScrolled = true;
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

            // подгрузка
            if (userScrolled && (visibleItemCount + pastVisiblesItems + 3) >= totalItemCount && dy > 0) {
                userScrolled = false;
                presenter.getMorePost();
            }
        }
    }
}
