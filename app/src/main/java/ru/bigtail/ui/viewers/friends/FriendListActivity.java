package ru.bigtail.ui.viewers.friends;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.vk.sdk.api.model.VKUsersArray;

import butterknife.BindView;
import ru.bigtail.R;
import ru.bigtail.ui.adapter.friend.FriendAdapter;
import ru.bigtail.vedroidMVP.PaginationListener;
import ru.bigtail.vedroidMVP.base.view.MVPActivity;

public class FriendListActivity extends MVPActivity<FriendListPresenter> implements PaginationListener.LoadListener {

    @BindView(R.id.loadFriendPB)
    ProgressBar loadFriendPB;

    @BindView(R.id.friendRV)
    RecyclerView friendRV;

    private FriendAdapter adapter;

    @Override
    protected void initUI() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        friendRV.setLayoutManager(layoutManager);
        adapter = new FriendAdapter();
        friendRV.setAdapter(adapter);
        friendRV.addOnScrollListener(new PaginationListener(layoutManager, this));
    }

    @Override
    public void loadMore() {
        presenter.loadData(adapter.getItemCount());
    }

    public void showLoading() {
        if (adapter.getItemCount() == 0) {
            loadFriendPB.setVisibility(View.VISIBLE);
        }
    }

    public void hideLoading() {
        loadFriendPB.setVisibility(View.GONE);
    }

    public void addFriend(VKUsersArray vkUsersArray) {
        adapter.addAll(vkUsersArray);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_friend_list;
    }

    @Override
    public FriendListPresenter getPresenterClass() {
        return new FriendListPresenter();
    }
}
