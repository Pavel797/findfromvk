package ru.bigtail.ui.viewers.mylike;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.bigtail.api.app.BigtailApiManager;
import ru.bigtail.api.app.BigtailErrorHelper;
import ru.bigtail.api.vk.VkApiHelper;
import ru.bigtail.model.answer.ModelAnswerUser;
import ru.bigtail.ui.adapter.list.UserListAdapter;

/**
 * Created by Павел on 22.08.2017.
 */

public class MyLikePresenter {

    private MyLikeActivity view;
    private UserListAdapter adapter;
    private Call<ModelAnswerUser> callMeLikes;

    public void attachView(MyLikeActivity view) {
        this.view = view;
        adapter = new UserListAdapter(view);
    }

    public UserListAdapter getAdapter() {
        return adapter;
    }

    public void onRefresh() {
        view.hideNullData();
        view.showProgressBar();
        adapter.clear();
        adapter.setEnd(false);
        loadResults(0);
    }

    public void getMorePost() {
        loadResults(adapter.getOffset());
    }

    public void loadResults(long offset) {
        callMeLikes = BigtailApiManager.meLikes(offset, new Callback<ModelAnswerUser>() {
            @Override
            public void onResponse(Call<ModelAnswerUser> call, Response<ModelAnswerUser> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ModelAnswerUser answer = response.body();
                    if (BigtailErrorHelper.error(answer.getErrorCode(), answer.getErrorMessage())) {
                        setDataInAdapter(answer);
                    }
                }
                view.hideProgressBar();
            }

            @Override
            public void onFailure(Call<ModelAnswerUser> call, Throwable t) {
                if (!call.isCanceled()) {
                    view.hideProgressBar();
                }
            }
        });
    }

    public void detachView() {
        if (callMeLikes != null)
            callMeLikes.cancel();
    }

    public void setDataInAdapter(ModelAnswerUser modelAnswerUser) {
        if (modelAnswerUser.getResponse().getItems().size() < VkApiHelper.count_get)
            adapter.setEnd(true);
        adapter.addUserCard(modelAnswerUser.getResponse().getItems());

        if (adapter.getOffset() == 0) {
            view.showNullData();
        }
    }
}
