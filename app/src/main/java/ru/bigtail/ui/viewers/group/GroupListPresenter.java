package ru.bigtail.ui.viewers.group;

import android.view.View;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import ru.bigtail.api.vk.VkApiHelper;
import ru.bigtail.ui.adapter.group.GroupAdapter;
import ru.bigtail.model.group.ModelAnswerGroup;

/**
 * Created by Павел on 18.08.2017.
 */

public class GroupListPresenter {

    private GroupListActivity view;
    private GroupAdapter adapter;

    public GroupListPresenter() {
    }

    public void attachView(GroupListActivity view) {
        this.view = view;
        adapter = new GroupAdapter(view);
    }

    public void detachView() {
        this.view = null;
    }

    public void getMorePost() {
        loadData(adapter.getOffset());
    }

    public void load() {
        view.loadGroupPB.setVisibility(View.VISIBLE);
        adapter.clear();
        loadData(adapter.getOffset());
    }

    public void loadData(long offset) {
        VkApiHelper.getMyGroups(offset, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();

                ModelAnswerGroup answer = gson.fromJson(response.json.toString(), ModelAnswerGroup.class);

                adapter.addItem(answer.getResponse().getItems());

                if (answer.getResponse().getItems().length == 0 || answer.getResponse().getItems().length  < VkApiHelper.count_get)
                    adapter.setEnd(true);

                view.loadGroupPB.setVisibility(View.GONE);
            }

            @Override
            public void onError(VKError error) {
                view.loadGroupPB.setVisibility(View.GONE);

                super.onError(error);
            }
        });
    }

    public GroupAdapter getAdapter() {
        return adapter;
    }
}
