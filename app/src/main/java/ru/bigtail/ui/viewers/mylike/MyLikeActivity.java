package ru.bigtail.ui.viewers.mylike;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.R;
import ru.bigtail.model.activity.SearchActivity;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.model.common.Types;
import ru.bigtail.model.fields.DataFields;
import ru.bigtail.model.fields.DataFieldsFriendsSearch;

public class MyLikeActivity extends AppCompatActivity implements SearchActivity {

    @BindView(R.id.loadUsersPB)
    ProgressBar loadUsersPB;

    @BindView(R.id.usersRV)
    RecyclerView usersRV;

    @BindView(R.id.titleTV)
    TextView titleTV;

    @BindView(R.id.myLikeNullLL)
    LinearLayout myLikeNullLL;

    @BindView(R.id.myLikeNullTV)
    TextView myLikeNullTV;

    private MyLikePresenter presenter;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_like);
        ButterKnife.bind(this);

        presenter = new MyLikePresenter();
        presenter.attachView(this);

        setUI();
    }

    private void setUI() {
        titleTV.setTypeface(AssetsManager.openSansLight);
        myLikeNullTV.setTypeface(AssetsManager.openSansRegular);

        layoutManager = new LinearLayoutManager(this);
        usersRV.setLayoutManager(layoutManager);
        usersRV.setAdapter(presenter.getAdapter());
        usersRV.addOnScrollListener(new MyScrollListener());

        presenter.onRefresh();
    }

    public void showProgressBar() {
        loadUsersPB.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        loadUsersPB.setVisibility(View.GONE);
    }


    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    public void openForm(DataFields fields) {
        Intent intent = new Intent();
        intent.putExtra(Types.link, ((DataFieldsFriendsSearch) fields).getLink());
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    public void showNullData() {
        myLikeNullLL.setVisibility(View.VISIBLE);
    }

    public void hideNullData() {
        myLikeNullLL.setVisibility(View.GONE);
    }

    private class MyScrollListener extends RecyclerView.OnScrollListener {

        private boolean userScrolled = false;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                userScrolled = true;
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

            // подгрузка
            if (userScrolled && (visibleItemCount + pastVisiblesItems + 3) >= totalItemCount && dy > 0) {
                userScrolled = false;
                presenter.getMorePost();
            }
        }
    }
}
