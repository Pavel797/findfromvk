package ru.bigtail.ui.viewers.friendslike;

import android.view.View;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.bigtail.api.app.BigtailApiManager;
import ru.bigtail.api.app.BigtailErrorHelper;
import ru.bigtail.model.answer.ModelAnswerUser;
import ru.bigtail.ui.adapter.friendslike.FriendLikeAdapter;

/**
 * Created by Павел on 23.08.2017.
 */

public class FriendsLikePresenter {

    private FriendsLikeActivity view;
    private FriendLikeAdapter adapter;
    private Call<ModelAnswerUser> callGetFriendsListWithLikes;

    public FriendsLikePresenter() {

    }

    public void attachView(FriendsLikeActivity view) {
        this.view = view;
        adapter = new FriendLikeAdapter();
    }

    public void getMorePost() {
        loadData(adapter.getOffset());
    }

    public void load() {
        view.loadFriendPB.setVisibility(View.VISIBLE);
        adapter.clear();
        loadData(adapter.getOffset());
    }

    public FriendLikeAdapter getAdapter() {
        return adapter;
    }

    public void loadData(long offset) {
        callGetFriendsListWithLikes = BigtailApiManager.getFriendsListWithLikes(offset, new Callback<ModelAnswerUser>() {
            @Override
            public void onResponse(Call<ModelAnswerUser> call, Response<ModelAnswerUser> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ModelAnswerUser answer = response.body();
                    if (BigtailErrorHelper.error(answer.getErrorCode(), answer.getErrorMessage())) {
                        setDataInAdapter(answer);
                    }
                }
                view.loadFriendPB.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ModelAnswerUser> call, Throwable t) {
                if (!call.isCanceled()) {
                    view.loadFriendPB.setVisibility(View.GONE);
                }
            }
        });
    }

    public void setDataInAdapter(ModelAnswerUser dataInAdapter) {
        adapter.addItem(dataInAdapter.getResponse().getItems());
    }

    public void detachView() {
        if (callGetFriendsListWithLikes != null)
            callGetFriendsListWithLikes.cancel();
    }
}
