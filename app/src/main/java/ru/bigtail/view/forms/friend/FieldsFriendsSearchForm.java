package ru.bigtail.view.forms.friend;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatImageButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.R;
import ru.bigtail.api.callback.SearchCallback;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.model.fields.DataFieldsFriendsSearch;

/**
 * Created by Павел on 29.07.2017.
 */

public class FieldsFriendsSearchForm {

    @BindView(R.id.linkTIL)
    TextInputLayout linkTIL;

    @BindView(R.id.nameTIL)
    TextInputLayout nameTIL;

    @BindView(R.id.checkLinkPB)
    ProgressBar checkLinkPB;

    @BindView(R.id.listFriendsBtn)
    AppCompatImageButton listFriendsBtn;

    private View view;
    private FieldsFriendsSearchFormPresenter presenter;

    public FieldsFriendsSearchForm(LinearLayout includeView, LayoutInflater layoutInflater) {
        this.view = layoutInflater.inflate(R.layout.friends_search_form_layout, includeView, false);
        ButterKnife.bind(this, view);

        linkTIL.getEditText().setTypeface(AssetsManager.openSansLight);
        nameTIL.getEditText().setTypeface(AssetsManager.openSansLight);

        checkLinkPB.setVisibility(View.GONE);

        presenter = new FieldsFriendsSearchFormPresenter();
        presenter.attachView(this);
    }

    public TextInputLayout getLinkTIL() {
        return linkTIL;
    }

    public TextInputLayout getNameTIL() {
        return nameTIL;
    }


    public View getView() {
        return view;
    }


    public void setErrorNull() {
        linkTIL.setError(null);
    }

    public void getDataFields(SearchCallback callback) {
        presenter.getDataFields(callback);
    }

    public void setErrorLink(int errorLink) {
        linkTIL.setError(view.getContext().getString(errorLink));
    }

    public void stopCheckLink() {
        checkLinkPB.setVisibility(View.GONE);
    }

    public void startCheckLink() {
        checkLinkPB.setVisibility(View.VISIBLE);
    }

    public AppCompatImageButton getListFriendsBtn() {
        return listFriendsBtn;
    }

    public void setField(DataFieldsFriendsSearch field) {
        linkTIL.getEditText().setText(field.getLink());
        nameTIL.getEditText().setText(field.getName());
    }
}