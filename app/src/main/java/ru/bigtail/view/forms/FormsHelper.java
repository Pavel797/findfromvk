package ru.bigtail.view.forms;

import android.view.LayoutInflater;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import ru.bigtail.api.callback.SearchCallback;
import ru.bigtail.model.common.Types;
import ru.bigtail.model.fields.DataFieldsFriendsSearch;
import ru.bigtail.model.fields.DataFieldsGlobalSearch;
import ru.bigtail.model.fields.DataFieldsGroupSearch;
import ru.bigtail.view.forms.friend.FieldsFriendsSearchForm;
import ru.bigtail.view.forms.global.FieldsGlobalSearchForm;
import ru.bigtail.view.forms.group.FieldsGroupSearchForm;
import ru.bigtail.R;
import ru.bigtail.model.fields.DataFields;

/**
 * Created by Павел on 15.07.2017.
 */

public class FormsHelper {

    private FieldsGlobalSearchForm fieldsGlobalSearchForm;
    private FieldsGroupSearchForm fieldsGroupSearchFormLayout;
    private FieldsFriendsSearchForm fieldsFriendsSearchForm;
    private LinearLayout includeView;
    private Animation fadeInAnimation;
    private Fields filds;

    public FormsHelper(LayoutInflater layoutInflater, LinearLayout includeView, DataFields savingFields) {
        this.includeView = includeView;
        this.fadeInAnimation = AnimationUtils.loadAnimation(layoutInflater.getContext(), R.anim.fadein);

        fieldsGlobalSearchForm = new FieldsGlobalSearchForm(includeView, layoutInflater);
        fieldsGroupSearchFormLayout = new FieldsGroupSearchForm(includeView, layoutInflater);
        fieldsFriendsSearchForm = new FieldsFriendsSearchForm(includeView, layoutInflater);

        if (savingFields != null) {
            switch (savingFields.getTypeFields()) {
                case Types.TYPE_FIELDS_FRIENDS:
                    fieldsFriendsSearchForm.setField((DataFieldsFriendsSearch) savingFields);
                    setSearchFriendFormLayout();
                    break;
                case Types.TYPE_FIELDS_GLOBAL:
                    fieldsGlobalSearchForm.setField((DataFieldsGlobalSearch) savingFields);
                    setSearchGlobalFormLayout();
                    break;
                case Types.TYPE_FIELDS_GROUP:
                    fieldsGroupSearchFormLayout.setField((DataFieldsGroupSearch) savingFields);
                    setSearchGroupFormLayout();
                    break;
            }
        }
    }

    public FieldsFriendsSearchForm getFieldsFriendsSearchForm() {
        return fieldsFriendsSearchForm;
    }

    public void setSearchGroupFormLayout() {
        removeAllViews();
        filds = Fields.GROUP;
        includeView.startAnimation(fadeInAnimation);
        includeView.addView(fieldsGroupSearchFormLayout.getView());
    }

    public void setSearchFriendFormLayout() {
        removeAllViews();
        filds = Fields.FRIENDS;
        includeView.startAnimation(fadeInAnimation);
        includeView.addView(fieldsFriendsSearchForm.getView());
    }

    public void setSearchGlobalFormLayout() {
        removeAllViews();
        filds = Fields.GLOBAL;
        includeView.startAnimation(fadeInAnimation);
        includeView.addView(fieldsGlobalSearchForm.getView());
    }

    private void removeAllViews() {
        setErrorNull();
        includeView.removeAllViews();
    }

    private void setErrorNull() {
        fieldsGlobalSearchForm.setErrorNull();
        fieldsFriendsSearchForm.setErrorNull();
        fieldsGroupSearchFormLayout.setErrorNull();
    }

    public void getDataFields(SearchCallback callback) {
        switch (filds) {
            case GLOBAL:
                fieldsGlobalSearchForm.getDataFields(callback);
                break;
            case GROUP:
                fieldsGroupSearchFormLayout.searchDataFields(callback);
                break;
            case FRIENDS:
                fieldsFriendsSearchForm.getDataFields(callback);
                break;
        }
    }

    public FieldsGroupSearchForm getFieldsGroupSearchForm() {
        return fieldsGroupSearchFormLayout;
    }

    private enum Fields {
        GROUP,
        FRIENDS,
        GLOBAL
    }
}
