package ru.bigtail.view.forms.group;

import android.content.res.Resources;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bigtail.model.fields.DataFieldsGroupSearch;
import ru.bigtail.model.forms.ModelCountry;
import ru.bigtail.ui.adapter.forms.CountriesAdapter;
import ru.bigtail.view.input.CityAutoCompleteTextView;
import ru.bigtail.R;
import ru.bigtail.api.callback.SearchCallback;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.ui.adapter.forms.CitiesAutoCompleteAdapter;

/**
 * Created by Павел on 29.07.2017.
 */

public class FieldsGroupSearchForm {

    private View view;
    private CountriesAdapter countriesAdapter;
    private CitiesAutoCompleteAdapter citiesAutoCompleteAdapter;
    private LayoutInflater layoutInflater;
    private FieldsGroupSearchFormPresenter presenter;

    @BindView(R.id.linkTIL)
    TextInputLayout linkTIL;

    @BindView(R.id.nameTIL)
    TextInputLayout nameTIL;

    @BindView(R.id.countriesSpinner)
    Spinner countriesSpinner;

    @BindView(R.id.countriesPB)
    ProgressBar countriesPB;

    @BindView(R.id.cityBP)
    ProgressBar cityBP;

    @BindView(R.id.checkLinkPB)
    ProgressBar checkLinkPB;

    @BindView(R.id.cityAutoCompleteTextView)
    CityAutoCompleteTextView cityAutoCompleteTextView;

    @BindView(R.id.cityFL)
    FrameLayout cityFL;

    @BindView(R.id.ageRSB)
    CrystalRangeSeekbar ageRSB;

    @BindView(R.id.ageTV)
    TextView ageTV;

    @BindView(R.id.allBtn)
    Button allBtn;

    @BindView(R.id.girlsBtn)
    Button girlsBtn;

    @BindView(R.id.manBtn)
    Button manBtn;

    @BindView(R.id.listGroupBtn)
    AppCompatImageButton listGroupBtn;

    @BindView(R.id.myInterestingTV)
    TextView myInterestingTV;

    @BindView(R.id.ageHintTV)
    TextView ageHintTV;

    public FieldsGroupSearchForm(LinearLayout includeView, LayoutInflater layoutInflater) {
        this.view = layoutInflater.inflate(R.layout.group_search_form_layout, includeView, false);
        this.layoutInflater = layoutInflater;
        ButterKnife.bind(this, view);

        presenter = new FieldsGroupSearchFormPresenter();
        presenter.attachView(this);

        initIU();
    }

    private void initIU() {
        ageHintTV.setTypeface(AssetsManager.openSansRegular);
        myInterestingTV.setTypeface(AssetsManager.openSansRegular);
        manBtn.setTypeface(AssetsManager.openSansRegular);
        girlsBtn.setTypeface(AssetsManager.openSansRegular);
        allBtn.setTypeface(AssetsManager.openSansRegular);
        ageTV.setTypeface(AssetsManager.openSansRegular);
        cityAutoCompleteTextView.setTypeface(AssetsManager.openSansRegular);
        nameTIL.getEditText().setTypeface(AssetsManager.openSansLight);
        linkTIL.getEditText().setTypeface(AssetsManager.openSansLight);

        initCitiesAutoCompleteAdapter();
        initCountriesSpinner();

        ageRSB.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                String str = String.valueOf(minValue.intValue()) + " - " + String.valueOf(maxValue.intValue());
                ageTV.setText(str);
            }
        });

        countriesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                presenter.onCountryClick(parent, itemSelected, selectedItemPosition, selectedId);

            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        View.OnClickListener ckListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.allBtn:
                        presenter.setAllSearch();
                        allBtn.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.radio_button_style));
                        girlsBtn.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.search_button_style));
                        manBtn.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.search_button_style));
                        break;
                    case R.id.girlsBtn:
                        presenter.setGirlsSearch();
                        girlsBtn.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.radio_button_style));
                        manBtn.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.search_button_style));
                        allBtn.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.search_button_style));
                        break;
                    case R.id.manBtn:
                        presenter.setManSearch();
                        manBtn.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.radio_button_style));
                        girlsBtn.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.search_button_style));
                        allBtn.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.search_button_style));
                        break;
                }
            }
        };

        allBtn.setOnClickListener(ckListener);
        girlsBtn.setOnClickListener(ckListener);
        manBtn.setOnClickListener(ckListener);

        ckListener.onClick(allBtn);

        checkLinkPB.setVisibility(View.GONE);
    }

    private void initCitiesAutoCompleteAdapter() {
        citiesAutoCompleteAdapter = new CitiesAutoCompleteAdapter(layoutInflater);
        cityAutoCompleteTextView = (CityAutoCompleteTextView) view.findViewById(R.id.cityAutoCompleteTextView);
        cityAutoCompleteTextView.setThreshold(2); // количество символово для старта поиска
        cityAutoCompleteTextView.setAdapter(citiesAutoCompleteAdapter);
        cityAutoCompleteTextView.setLoadingIndicator(cityBP);
        cityAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                presenter.onCityClick(adapterView, view, position, id);
            }
        });
    }

    public AppCompatImageButton getListGroupBtn() {
        return listGroupBtn;
    }

    private void initCountriesSpinner() {
        countriesPB.setVisibility(View.VISIBLE);
        presenter.initCountriesSpinner();
    }

    public void setCountriesAdapter(List<ModelCountry> arrCountry) {
        countriesAdapter = new CountriesAdapter(layoutInflater, android.R.layout.simple_dropdown_item_1line, arrCountry);
        countriesSpinner.setAdapter(countriesAdapter);
        countriesPB.setVisibility(View.GONE);
    }

    public void setErrorNull() {
        linkTIL.setError(null);
    }

    public View getView() {
        return view;
    }

    public Resources getResources() {
        return view.getResources();
    }

    public void setCityTitle(String cityTitle) {
        cityAutoCompleteTextView.setText(cityTitle);
    }

    public CountriesAdapter getCountriesAdapter() {
        return countriesAdapter;
    }

    public CitiesAutoCompleteAdapter getCitiesAutoCompleteAdapter() {
        return citiesAutoCompleteAdapter;
    }

    public void setCityVisibility(int cityVisibility) {
        cityFL.setVisibility(cityVisibility);
    }

    public void searchDataFields(SearchCallback callback) {
        presenter.searchDataFields(callback);
    }

    public void setErrorLink(int errorLink) {
        linkTIL.setError(view.getContext().getString(errorLink));
        linkTIL.setFocusable(true);
    }

    public void startCheckLink() {
        checkLinkPB.setVisibility(View.VISIBLE);
    }

    public void stopCheckLink() {
        checkLinkPB.setVisibility(View.GONE);
    }

    public void setField(DataFieldsGroupSearch field) {
        linkTIL.getEditText().setText(field.getLink());
        nameTIL.getEditText().setText(field.getName());
        /*ageRSB.setMinStartValue(field.getAgeFrom())
                .setMaxStartValue(field.getAgeTo()).apply();*/
        switch (field.getSex()) {
            case 0:
                allBtn.callOnClick();
                break;
            case 1:
                girlsBtn.callOnClick();
                break;
            case 2:
                manBtn.callOnClick();
                break;
        }
    }
}