package ru.bigtail.view.forms.friend;

import ru.bigtail.api.callback.CheckFieldsCallback;
import ru.bigtail.R;
import ru.bigtail.api.callback.SearchCallback;
import ru.bigtail.api.vk.VkApiHelper;
import ru.bigtail.model.fields.DataFieldsFriendsSearch;

/**
 * Created by Павел on 02.08.2017.
 */

public class FieldsFriendsSearchFormPresenter {

    private FieldsFriendsSearchForm view;

    public FieldsFriendsSearchFormPresenter() {
    }

    public void attachView(FieldsFriendsSearchForm view) {
        this.view = view;
    }

    public void detachView() {
        this.view = null;
    }

    public void getDataFields(final SearchCallback callback) {
        if (view.linkTIL.getEditText().getText().toString() == null ||
                view.linkTIL.getEditText().getText().toString().isEmpty()){
            view.setErrorLink(R.string.error_empty_field);
            return;
        }

        view.startCheckLink();
        checkLink(view.linkTIL.getEditText().getText().toString(), new CheckFieldsCallback() {
            @Override
            public void goodFields() {
                view.setErrorNull();
                view.stopCheckLink();
                callback.search(new DataFieldsFriendsSearch(
                        view.linkTIL.getEditText().getText().toString(),
                        view.nameTIL.getEditText().getText().toString()));
            }

            @Override
            public void errorFields() {
                view.stopCheckLink();
                view.setErrorLink(R.string.error_link);
            }
        });
    }

    public void checkLink(String link, CheckFieldsCallback callback) {
        VkApiHelper.checkLink(link, callback, "user");
    }
}
