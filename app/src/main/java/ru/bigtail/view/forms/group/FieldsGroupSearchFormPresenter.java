package ru.bigtail.view.forms.group;

import android.view.View;
import android.widget.AdapterView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import ru.bigtail.api.callback.CheckFieldsCallback;
import ru.bigtail.model.fields.DataFieldsGroupSearch;
import ru.bigtail.model.forms.ModelCity;
import ru.bigtail.model.forms.ModelCountry;
import ru.bigtail.R;
import ru.bigtail.api.callback.SearchCallback;
import ru.bigtail.api.vk.VkApiHelper;
import ru.bigtail.model.answer.ModelAnswerCountry;

/**
 * Created by Павел on 31.07.2017.
 */

public class FieldsGroupSearchFormPresenter {

    private FieldsGroupSearchForm view;
    private Integer cityId = null, countriesId = null;
    private int whenSearch = 0;

    public FieldsGroupSearchFormPresenter() {
    }

    public void attachView(FieldsGroupSearchForm view) {
        this.view = view;
    }

    public void detachView() {
        this.view = null;
    }

    public void onCityClick(AdapterView<?> adapterView, View view, int position, long id) {
        ModelCity city = (ModelCity) adapterView.getItemAtPosition(position);
        cityId = city.getId();
        this.view.setCityTitle(city.getTitle());
    }

    public void initCountriesSpinner() {
        VkApiHelper.getCountries(18, 0, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                ModelAnswerCountry answer = gson.fromJson(response.json.toString(), ModelAnswerCountry.class);

                List<ModelCountry> arrCountry = new ArrayList<>();
                arrCountry.add(new ModelCountry("Страна...", 0));

                Collections.addAll(arrCountry, answer.getResponse().getItems());

                view.setCountriesAdapter(arrCountry);

                super.onComplete(response);
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                view.setCountriesAdapter(Arrays.asList(new ModelCountry(view.getResources().getString(R.string.error_countries), -2),
                        new ModelCountry(view.getResources().getString(R.string.try_agen), -1)));
                super.attemptFailed(request, attemptNumber, totalAttempts);
            }

            @Override
            public void onError(VKError error) {
                view.setCountriesAdapter(Arrays.asList(new ModelCountry(view.getResources().getString(R.string.error_countries), -2),
                        new ModelCountry(view.getResources().getString(R.string.try_agen), -1)));
                super.onError(error);
            }
        });
    }

    public void onCountryClick(AdapterView<?> parent,
                               View itemSelected, int selectedItemPosition, long selectedId) {

        this.view.setCityTitle(null);

        if (this.view.getCountriesAdapter() == null) {
            countriesId = null;
            this.view.setCityVisibility(View.GONE);
            return;
        }
        ModelCountry country = this.view.getCountriesAdapter().getItem(selectedItemPosition);
        if (country == null) {
            countriesId = null;
            this.view.setCityVisibility(View.GONE);
            return;
        }

        int id = country.getId();

        if (id == 0) {
            countriesId = null;
            this.view.setCityVisibility(View.GONE);
        } else if (id == -1) {
            this.view.setCityVisibility(View.GONE);
            initCountriesSpinner();
            countriesId = null;
        } else if (id == -2) {
            this.view.setCityVisibility(View.GONE);
            countriesId = null;
        } else {
            this.view.setCityVisibility(View.VISIBLE);
            countriesId = selectedItemPosition;
            this.view.getCitiesAutoCompleteAdapter().setCountryId(countriesId);
        }
    }

    public void setAllSearch() {
        whenSearch = 0;
    }

    public void setGirlsSearch() {
        whenSearch = 1;
    }

    public void setManSearch() {
        whenSearch = 2;
    }

    public void searchDataFields(final SearchCallback callback) {
        if (view.linkTIL.getEditText().getText().toString() == null ||
                view.linkTIL.getEditText().getText().toString().isEmpty()){
            view.setErrorLink(R.string.error_empty_field);
            return;
        }

        view.startCheckLink();
        checkLink(view.linkTIL.getEditText().getText().toString(), new CheckFieldsCallback() {
            @Override
            public void goodFields() {
                view.setErrorNull();
                view.stopCheckLink();
                callback.search(new DataFieldsGroupSearch(whenSearch,
                        view.nameTIL.getEditText().getText().toString(),
                        view.linkTIL.getEditText().getText().toString(),
                        view.ageRSB.getSelectedMinValue().intValue(),
                        view.ageRSB.getSelectedMaxValue().intValue(),
                        cityId,
                        countriesId));
            }

            @Override
            public void errorFields() {
                view.stopCheckLink();
                view.setErrorLink(R.string.error_link);
            }
        });
    }

    public void checkLink(String link, CheckFieldsCallback callback) {
        VkApiHelper.checkLink(link, callback, "group");
    }
}
