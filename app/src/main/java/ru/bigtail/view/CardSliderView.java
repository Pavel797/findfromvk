package ru.bigtail.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.kaelaela.verticalviewpager.transforms.ZoomOutTransformer;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.bigtail.api.app.BigtailApiManager;
import ru.bigtail.api.app.BigtailErrorHelper;
import ru.bigtail.model.answer.ModelAnswerLike;
import ru.bigtail.model.assets.AssetsManager;
import ru.bigtail.model.fields.DataFieldsFriendsSearch;
import ru.bigtail.model.items.ModelPhoto;
import ru.bigtail.model.items.ModelUser;
import ru.bigtail.ui.adapter.card.page.ContentPageAdapter;
import ru.bigtail.ui.main.activity.MainActivity;
import ru.bigtail.util.TextFormatter;
import ru.bigtail.view.custom.VerticalViewPager;
import ru.bigtail.R;

/**
 * Created by Павел on 02.08.2017.
 */

public class CardSliderView {

    @BindView(R.id.photoVP)
    VerticalViewPager photoVP;

    @BindView(R.id.pageIndicator)
    CircleIndicator pageIndicator;

    @BindView(R.id.counterTotalLikeTV)
    TextView counterTotalLikeTV;

    @BindView(R.id.nameTV)
    TextView nameTV;

    @BindView(R.id.linkProfileAIV)
    AppCompatImageView linkProfileAIV;

    @BindView(R.id.linkInFormACIV)
    AppCompatImageView linkInFormACIV;

    @BindView(R.id.infoTV)
    TextView infoTV;

    @BindView(R.id.likesLL)
    LinearLayout likesLL;

    @BindView(R.id.likeIV)
    AppCompatImageView likeIV;

    private ContentPageAdapter adapter;
    private View rootView;
    private MainActivity activity;

    public CardSliderView(Activity activity, ViewGroup group) {
        rootView = activity.getLayoutInflater().inflate(R.layout.layout_card, group, false);
        ButterKnife.bind(this, rootView);

        this.activity = (MainActivity) activity;

        adapter = new ContentPageAdapter();
        adapter.attach(rootView.getContext());
        setUI();
    }

    private void setUI() {
        counterTotalLikeTV.setTypeface(AssetsManager.openSansLight);
        nameTV.setTypeface(AssetsManager.openSansLight);
        infoTV.setTypeface(AssetsManager.openSansLight);

        photoVP.setPageTransformer(true, new ZoomOutTransformer());
    }

    public void setModelUserCard(final ModelUser modelUser) {
        adapter.clear();
        adapter.addLink(modelUser.getPhoto_max_orig());
        if (modelUser.getLinksPhoto() != null) {
            for (ModelPhoto i : modelUser.getLinksPhoto()) {
                adapter.addLink(i.getPhoto_604());
            }
        }
        photoVP.setAdapter(adapter);
        pageIndicator.setViewPager(photoVP);

        if (modelUser.getIs_liked()) {
            counterTotalLikeTV.setTextColor(rootView.getResources().getColor(R.color.red));
            likeIV.setImageDrawable(ContextCompat.getDrawable(rootView.getContext(), R.drawable.ic_heart_active));
        } else {
            counterTotalLikeTV.setTextColor(rootView.getResources().getColor(R.color.whiteGrey));
            likeIV.setImageDrawable(ContextCompat.getDrawable(rootView.getContext(), R.drawable.ic_heart_passive));
        }

        infoTV.setText(modelUser.getInfo());

        nameTV.setText(modelUser.getFirst_name() + " " + modelUser.getLast_name());
        counterTotalLikeTV.setText(TextFormatter.getFormatCounterLikes(modelUser.getLikes_count_total()));

        final View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.likesLL:
                        clickLike(modelUser.getId());
                        break;
                    case R.id.linkProfileAIV:
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(activity.getString(R.string.baseVkURL) + modelUser.getScreen_name()));
                        activity.startActivity(intent);
                        break;

                    case R.id.linkInFormACIV:
                        activity.openForm(new DataFieldsFriendsSearch(activity.getString(R.string.baseVkURL) + modelUser.getScreen_name(), ""));
                        break;
                }
            }
        };

        linkInFormACIV.setOnClickListener(clickListener);
        linkProfileAIV.setOnClickListener(clickListener);
        likesLL.setOnClickListener(clickListener);

        adapter.notifyDataSetChanged();
    }

    private void clickLike(Long id) {
        likeIV.startAnimation(AnimationUtils.loadAnimation(rootView.getContext(), R.anim.rotate_center));
        BigtailApiManager.likesClick(id, new Callback<ModelAnswerLike>() {
            @Override
            public void onResponse(Call<ModelAnswerLike> call, Response<ModelAnswerLike> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ModelAnswerLike answer = response.body();
                    if (BigtailErrorHelper.error(answer.getErrorCode(), answer.getErrorMessage())) {
                        if (answer.getResponse().getIsLiked()) {
                            likeIV.setImageDrawable(ContextCompat.getDrawable(rootView.getContext(), R.drawable.ic_heart_active));
                        } else {
                            likeIV.setImageDrawable(ContextCompat.getDrawable(rootView.getContext(), R.drawable.ic_heart_passive));
                        }
                    }
                    counterTotalLikeTV.setText(String.valueOf(response.body().getResponse().getLikesCountTotal()));
                } else {
                    BigtailErrorHelper.requestNotSuccessful(response.code(), response.message());
                }

                likeIV.clearAnimation();
                YoYo.with(Techniques.Pulse)
                        .duration(500)
                        .playOn(likeIV);
            }

            @Override
            public void onFailure(Call<ModelAnswerLike> call, Throwable t) {
                if (!call.isCanceled()) {
                    likeIV.clearAnimation();
                    BigtailErrorHelper.onFailure(t);
                }
            }
        });
    }

    public View getView() {
        return rootView;
    }
}
