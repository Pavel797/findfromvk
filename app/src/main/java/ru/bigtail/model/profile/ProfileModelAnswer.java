package ru.bigtail.model.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Павел on 21.08.2017.
 */

public class ProfileModelAnswer {

    @SerializedName("result")
    @Expose
    private Integer result;

    @SerializedName("response")
    @Expose
    private ProfileModel response;

    @SerializedName("error_message")
    @Expose
    private String error_message;

    @SerializedName("error_code")
    @Expose
    private int error_code;

    public ProfileModelAnswer() {

    }

    public Integer getResult() {
        return result;
    }

    public ProfileModel getResponse() {
        return response;
    }

    public String getErrorMessage() {
        return error_message;
    }

    public int getErrorCode() {
        return error_code;
    }
}
