package ru.bigtail.model.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Павел on 21.08.2017.
 */

public class ProfileModel {

    @SerializedName("likes_count_from_man")
    @Expose
    private Integer likes_count_from_man;

    @SerializedName("likes_count_from_women")
    @Expose
    private Integer likes_count_from_women;

    @SerializedName("likes_count_total")
    @Expose
    private Integer likes_count_total;

    @SerializedName("photo_100")
    @Expose
    private String photo_100;

    @SerializedName("first_name")
    @Expose
    String first_name;

    @SerializedName("last_name")
    @Expose
    String last_name;

    public ProfileModel() {
    }

    public Integer getLikesCountFromMan() {
        return likes_count_from_man;
    }

    public Integer getLikesCountFromWomen() {
        return likes_count_from_women;
    }

    public Integer getLikesCountTotal() {
        return likes_count_total;
    }

    public String getPhoto100() {
        return photo_100;
    }

    public String getFirstName() {
        return first_name;
    }

    public String getLastName() {
        return last_name;
    }
}
