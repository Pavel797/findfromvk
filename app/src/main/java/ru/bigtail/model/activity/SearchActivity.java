package ru.bigtail.model.activity;

import android.content.Intent;

import ru.bigtail.model.fields.DataFields;

/**
 * Created by Павел on 22.08.2017.
 */

public interface SearchActivity {

    void openForm(DataFields fields);
    void startActivity(Intent intent);
}
