package ru.bigtail.model.forms;

/**
 * Created by Павел on 16.07.2017.
 */

public class ModelCountry {

    private String title;
    private int id;

    public ModelCountry() {
    }

    public ModelCountry(String title, int id) {
        this.id = id;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }
}
