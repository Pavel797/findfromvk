package ru.bigtail.model.forms;

/**
 * Created by Павел on 16.07.2017.
 */

public class ModelCity {

    private String title;
    private int id;
    private int important;

    public ModelCity() {
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }

    public int getImportant() {
        return important;
    }
}
