package ru.bigtail.model.fields;

/**
 * Created by Павел on 16.07.2017.
 */

public interface DataFields {
    int getTypeFields();
    void setIgnoreHistory(boolean val);
    boolean isIgnoreHistory();
    String getHistoryId();
    void setHistoryId(String id);
    int getOffset();
    void setOffset(int offset);
}
