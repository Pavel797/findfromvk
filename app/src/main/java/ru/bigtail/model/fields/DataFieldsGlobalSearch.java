package ru.bigtail.model.fields;

import ru.bigtail.model.common.Types;

/**
 * Created by Павел on 16.07.2017.
 */

public class DataFieldsGlobalSearch implements DataFields{

    private String name;
    private Integer sex, ageFrom, ageTo, cityId, countriesId; // 0 - всех, 1 - бабу, 2 - мужика
    private boolean ignore_history;
    private String history_id;
    private int offset;

    public DataFieldsGlobalSearch(Integer sex, String name, Integer ageFrom,
                                  Integer ageTo, Integer cityId, Integer countriesId) {
        this.name = name;
        this.sex = sex;
        this.ageFrom = ageFrom;
        this.ageTo = ageTo;
        this.cityId = cityId;
        this.countriesId = countriesId;
        ignore_history = false;
        offset = 0;
    }

    public Integer getCityId() {
        return cityId;
    }

    public Integer getCountriesId() {
        return countriesId;
    }

    public String getName() {
        return name;
    }

    public Integer getSex() {
        return sex;
    }

    public Integer getAgeFrom() {
        return ageFrom;
    }

    public Integer getAgeTo() {
        return ageTo;
    }


    @Override
    public boolean isIgnoreHistory() {
        return ignore_history;
    }

    @Override
    public String getHistoryId() {
        return history_id;
    }

    @Override
    public void setHistoryId(String id) {
        history_id = id;
    }

    @Override
    public int getOffset() {
        return offset;
    }

    @Override
    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Override
    public void setIgnoreHistory(boolean val) {
        ignore_history = val;
    }

    @Override
    public int getTypeFields() {
        return Types.TYPE_FIELDS_GLOBAL;
    }
}
