package ru.bigtail.model.fields;

import ru.bigtail.model.common.Types;

/**
 * Created by Павел on 16.07.2017.
 */

public class DataFieldsGroupSearch implements DataFields {

    private String name, link;
    private Integer sex, ageFrom, ageTo, cityId, countriesId; // 0 - всех, 1 - бабу, 2 - мужика
    private boolean ignore_history;
    private String history_id;
    private int offset;

    public DataFieldsGroupSearch(Integer sex, String name, String link,
                                 Integer ageFrom, Integer ageTo, Integer cityId, Integer countriesId) {
        this.name = name;
        this.link = link;
        this.sex = sex;
        this.ageFrom = ageFrom;
        this.ageTo = ageTo;
        this.cityId = cityId;
        this.countriesId = countriesId;
        offset = 0;
        ignore_history = false;
    }


    @Override
    public boolean isIgnoreHistory() {
        return ignore_history;
    }

    @Override
    public String getHistoryId() {
        return history_id;
    }

    @Override
    public void setHistoryId(String id) {
        history_id = id;
    }

    @Override
    public int getOffset() {
        return offset;
    }

    @Override
    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Override
    public void setIgnoreHistory(boolean val) {
        ignore_history = val;
    }

    public String getLink() {
        return link;
    }

    public Integer getCityId() {
        return cityId;
    }

    public Integer getCountriesId() {
        return countriesId;
    }

    public String getName() {
        return name;
    }

    public Integer getSex() {
        return sex;
    }

    public Integer getAgeFrom() {
        return ageFrom;
    }

    public Integer getAgeTo() {
        return ageTo;
    }

    @Override
    public int getTypeFields() {
        return Types.TYPE_FIELDS_GROUP;
    }

}
