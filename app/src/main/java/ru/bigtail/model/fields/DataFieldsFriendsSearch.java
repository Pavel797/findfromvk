package ru.bigtail.model.fields;

import ru.bigtail.model.common.Types;

/**
 * Created by Павел on 15.07.2017.
 */

public class DataFieldsFriendsSearch implements DataFields {

    private String link, name;
    private boolean ignore_history;
    private String history_id;
    private int offset;

    public DataFieldsFriendsSearch(String link, String name) {
        this.link = link;
        this.name = name;
        ignore_history = false;
        offset = 0;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean isIgnoreHistory() {
        return ignore_history;
    }

    @Override
    public String getHistoryId() {
        return history_id;
    }

    @Override
    public void setHistoryId(String id) {
        history_id = id;
    }

    @Override
    public int getOffset() {
        return offset;
    }

    @Override
    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Override
    public void setIgnoreHistory(boolean val) {
        ignore_history = val;
    }

    @Override
    public int getTypeFields() {
        return Types.TYPE_FIELDS_FRIENDS;
    }
}
