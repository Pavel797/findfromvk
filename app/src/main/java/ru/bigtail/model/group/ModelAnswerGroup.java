package ru.bigtail.model.group;

/**
 * Created by Павел on 18.08.2017.
 */

public class ModelAnswerGroup {
    private ModelResponse response;

    public ModelAnswerGroup() {}

    public ModelResponse getResponse() {
        return response;
    }

    public void setResponse(ModelResponse response) {
        this.response = response;
    }

    public class ModelResponse {

        private long count;
        private ModelGroup[] items;

        public ModelResponse() {}

        public long getCount() {
            return count;
        }

        public ModelGroup[] getItems() {
            return items;
        }
    }
}
