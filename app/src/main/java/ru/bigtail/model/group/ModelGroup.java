package ru.bigtail.model.group;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Павел on 18.08.2017.
 */

public class ModelGroup {

    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("screen_name")
    @Expose
    private String screen_name;

    @SerializedName("is_closed")
    @Expose
    private int is_closed;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("is_admin")
    @Expose
    private int is_admin;

    @SerializedName("count")
    @Expose
    private int count;

    @SerializedName("photo_50")
    @Expose
    private String photo_50;

    @SerializedName("photo_100")
    @Expose
    private String photo_100;

    @SerializedName("photo_200")
    @Expose
    private String photo_200;

    @SerializedName("is_author")
    @Expose
    private int is_author;

    public ModelGroup() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public int getIs_closed() {
        return is_closed;
    }

    public String getType() {
        return type;
    }

    public int getIs_admin() {
        return is_admin;
    }

    public int getCount() {
        return count;
    }

    public String getPhoto_50() {
        return photo_50;
    }

    public String getPhoto_100() {
        return photo_100;
    }

    public String getPhoto_200() {
        return photo_200;
    }

    public int getIs_author() {
        return is_author;
    }
}
