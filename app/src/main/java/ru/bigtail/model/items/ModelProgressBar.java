package ru.bigtail.model.items;

import ru.bigtail.model.common.Types;
import ru.bigtail.model.Item;

/**
 * Created by Павел on 14.07.2017.
 */

public class ModelProgressBar implements CustomCardItem, Item {
    @Override
    public int getTypeView() {
        return Types.TYPE_PROGRESS_BAR;
    }
}
