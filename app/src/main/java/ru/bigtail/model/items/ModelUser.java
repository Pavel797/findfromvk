package ru.bigtail.model.items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bigtail.model.forms.ModelCity;
import ru.bigtail.model.common.Types;
import ru.bigtail.model.forms.ModelCountry;
import ru.bigtail.util.TextFormatter;

/**
 * Created by Павел on 14.07.2017.
 */

public class ModelUser implements CustomCardItem {

    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("online")
    @Expose
    private int online;

    @SerializedName("first_name")
    @Expose
    private String first_name;

    @SerializedName("last_name")
    @Expose
    private String last_name;

    @SerializedName("screen_name")
    @Expose
    private String screen_name;

    @SerializedName("photo_50")
    @Expose
    private String photo_50;

    @SerializedName("photo_100")
    @Expose
    private String photo_100;

    @SerializedName("photo_200")
    @Expose
    private String photo_200;

    @SerializedName("photo_max")
    @Expose
    private String photo_max;

    @SerializedName("photo_200_orig")
    @Expose
    private String photo_200_orig;

    @SerializedName("photo_400_orig")
    @Expose
    private String photo_400_orig;

    @SerializedName("photo_max_orig")
    @Expose
    private String photo_max_orig;

    @SerializedName("links_photo")
    @Expose
    private ArrayList<ModelPhoto> links_photo;

    @SerializedName("city")
    @Expose
    private ModelCity city;

    @SerializedName("country")
    @Expose
    private ModelCountry country;

    @SerializedName("bdate")
    @Expose
    private String bdate;

    @SerializedName("is_liked")
    @Expose
    private Boolean is_liked;

    @SerializedName("likes_count_from_man")
    @Expose
    private Integer likes_count_from_man;

    @SerializedName("likes_count_from_women")
    @Expose
    private Integer likes_count_from_women;

    @SerializedName("likes_count_total")
    @Expose
    private Integer likes_count_total;

    public ModelUser() {
        links_photo = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public int getOnline() {
        return online;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public String getPhoto_50() {
        return photo_50;
    }

    public String getPhoto_100() {
        return photo_100;
    }

    public String getPhoto_200() {
        return photo_200;
    }

    public String getPhoto_max() {
        return photo_max;
    }

    public String getPhoto_200_orig() {
        return photo_200_orig;
    }

    public String getPhoto_400_orig() {
        return photo_400_orig;
    }

    public String getPhoto_max_orig() {
        return photo_max_orig;
    }

    public long getLikes_count_total() {
        return likes_count_total;
    }

    public Boolean getIs_liked() {
        return is_liked;
    }

    public ModelCity getCity() {
        return city;
    }

    public ModelCountry getCountry() {
        return country;
    }

    public String getBdate() {
        return bdate;
    }

    public Integer getLikesCountMan() {
        return likes_count_from_man;
    }

    public Integer getLikesCountWomen() {
        return likes_count_from_women;
    }

    public String getInfo() {
        return TextFormatter.getFormatInfo(this);
    }

    @Override
    public int getTypeView() {
        return Types.TYPE_CARD;
    }

    public ArrayList<ModelPhoto> getLinksPhoto() {
        return links_photo;
    }
}
