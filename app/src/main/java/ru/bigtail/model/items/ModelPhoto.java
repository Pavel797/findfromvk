package ru.bigtail.model.items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Павел on 14.07.2017.
 */

public class ModelPhoto {

    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("album_id")
    @Expose
    private long album_id;

    @SerializedName("owner_id")
    @Expose
    private long owner_id;

    @SerializedName("date")
    @Expose
    private long date;

    @SerializedName("width")
    @Expose
    private int width;

    @SerializedName("height")
    @Expose
    private int height;

    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("photo_75")
    @Expose
    private String photo_75;

    @SerializedName("photo_130")
    @Expose
    private String photo_130;

    @SerializedName("photo_604")
    @Expose
    private String photo_604;

    @SerializedName("photo_807")
    @Expose
    private String photo_807;

    public ModelPhoto() {
    }

    public ModelPhoto(String uslPhoto, int width, int height) {
        this.width = width;
        this.height = height;
        photo_75 = uslPhoto;
        photo_130 = uslPhoto;
        photo_604 = uslPhoto;
        photo_807 = uslPhoto;
        id = -1;
        album_id = -1;
        owner_id = -1;
        date = -1;
    }

    public long getId() {
        return id;
    }

    public long getAlbum_id() {
        return album_id;
    }

    public long getOwner_id() {
        return owner_id;
    }

    public long getDate() {
        return date;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getText() {
        return text;
    }

    public String getPhoto_75() {
        return photo_75;
    }

    public String getPhoto_130() {
        return photo_130;
    }

    public String getPhoto_604() {
        return photo_604;
    }

    public String getPhoto_807() {
        return photo_807;
    }
}
