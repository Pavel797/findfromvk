package ru.bigtail.model.assets;

import android.content.res.AssetManager;
import android.graphics.Typeface;

/**
 * Created by Павел on 19.08.2017.
 */

public class AssetsManager {

    public static Typeface openSansLight;
    public static Typeface openSansRegular;

    public static void init(AssetManager assets) {
        openSansLight = Typeface.createFromAsset(assets, "fonts/OpenSans-Light.ttf");
        openSansRegular = Typeface.createFromAsset(assets, "fonts/OpenSans-Regular.ttf");
    }
}
