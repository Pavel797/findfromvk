package ru.bigtail.model.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Павел on 13.08.2017.
 */

public class HistoryObject {

    @SerializedName("q")
    @Expose
    private String q;

    @SerializedName("screen_name")
    @Expose
    private String screen_name;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("age_from")
    @Expose
    private String age_from;

    @SerializedName("age_to")
    @Expose
    private String age_to;

    @SerializedName("sex")
    @Expose
    private String sex;

    public HistoryObject() {
    }

    public String getQ() {
        return q;
    }

    public String getScreenName() {
        return screen_name;
    }

    public Integer getCountry() {
        if (country == null)
            return null;
        return Integer.parseInt(country);
    }

    public Integer getCity() {
        if (city == null)
            return null;
        return Integer.parseInt(city);
    }

    public Integer getAge_from() {
        if (age_from == null)
            return null;
        return Integer.parseInt(age_from);
    }

    public Integer getAge_to() {
        if (age_to == null)
            return null;
        return Integer.parseInt(age_to);
    }

    public Integer getSex() {
        if (sex == null)
            return null;
        return Integer.parseInt(sex);
    }
}
