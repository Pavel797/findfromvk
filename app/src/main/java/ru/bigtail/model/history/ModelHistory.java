package ru.bigtail.model.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.bigtail.model.Item;
import ru.bigtail.model.common.Types;

/**
 * Created by Павел on 13.08.2017.
 */

public class ModelHistory implements Item {

    @SerializedName("history_id")
    @Expose
    private String history_id;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("offset")
    @Expose
    private int offset;

    @SerializedName("date")
    @Expose
    private long date;

    @SerializedName("history_object")
    @Expose
    private HistoryObject history_object;

    public ModelHistory() {
    }

    public String getHistoryId() {
        return history_id;
    }

    public String getType() {
        return type;
    }

    public int getOffset() {
        return offset;
    }

    public long getDate() {
        return date;
    }

    public HistoryObject getHistoryObject() {
        return history_object;
    }

    @Override
    public int getTypeView() {
        return Types.TYPE_HISTORY;
    }
}
