package ru.bigtail.model.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Павел on 13.08.2017.
 */

public class ModelAnswerHistoryDelete {

    @SerializedName("result")
    @Expose
    private Integer result;

    @SerializedName("response")
    @Expose
    private ModelDeleteHistory response;

    @SerializedName("error_message")
    @Expose
    private String error_message;

    @SerializedName("error_code")
    @Expose
    private int error_code;

    public int getErrorCode() {
        return error_code;
    }

    class ModelDeleteHistory {

        @SerializedName("deleted")
        @Expose
        public Boolean deleted;

        public ModelDeleteHistory() {
        }
    }

    public Integer getResult() {
        return result;
    }

    public String getErrorMessage() {
        return error_message;
    }

    public Boolean isDeleted() {
        return response.deleted;
    }
}
