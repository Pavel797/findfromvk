package ru.bigtail.model.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Павел on 13.08.2017.
 */

public class ModelAnswerHistory {

    @SerializedName("result")
    @Expose
    private Integer result;

    @SerializedName("response")
    @Expose
    private ArrayList<ModelHistory> response;

    @SerializedName("error_message")
    @Expose
    private String error_message;

    @SerializedName("error_code")
    @Expose
    private int error_code;

    public ModelAnswerHistory() {

    }

    public Integer getResult() {
        return result;
    }

    public ArrayList<ModelHistory> getResponse() {
        return response;
    }

    public String getErrorMessage() {
        return error_message;
    }

    public int getErrorCode() {
        return error_code;
    }
}
