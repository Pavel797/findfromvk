package ru.bigtail.model.common;

/**
 * Created by Павел on 14.07.2017.
 */

public class Types {
    public static final int TYPE_CARD = 1;
    public static final int TYPE_PROGRESS_BAR = 2;
    public static final int TYPE_FIELDS_FRIENDS = 3;
    public static final int TYPE_FIELDS_GLOBAL = 4;
    public static final int TYPE_FIELDS_GROUP = 5;
    public static final int TYPE_USER_VIEW = 6;
    public static final int TYPE_HISTORY = 7;

    public static final int TYPE_FRIENDS = 7;
    public static final int TYPE_GROUP = 8;


    public static final String Type_global = "global";
    public static final String Type_friend = "friend";
    public static final String Type_group = "group";

    public static final String link = "link";
}
