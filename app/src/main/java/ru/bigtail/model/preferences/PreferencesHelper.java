package ru.bigtail.model.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.securepreferences.SecurePreferences;

/**
 * Created by Павел on 13.07.2017.
 */

public class PreferencesHelper {

    public static final String YM_API_KEY = "c8da048b-3919-4c1c-87a0-9647d9d2c7ee";

    private static final String STORAGE_NAME = "find_from_vk";
    private static final String FLAG_IS_FIRST_APPLICATION_LAUNCH = "is_first_application_launch";
    private static final String VIEW_IN_CARDS = "view_in_cards";
    private static final String VIEW_IN_LIST = "view_in_list";

    private static SharedPreferences settings = null;

    public static void init(Context context) {
        settings = new SecurePreferences(context, YM_API_KEY, STORAGE_NAME);
    }

    private static void check() {
        if (settings == null)
            throw new ExceptionInInitializerError("PreferencesHelper must be init");
    }

    private static void addProperty(String name, boolean value) {
        check();
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(name, value);
        editor.apply();
    }

    private static void addProperty(String name, String value) {
        check();
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, value);
        editor.apply();
    }

    public static void setFirstApplicationLaunch() {
        check();
        addProperty(FLAG_IS_FIRST_APPLICATION_LAUNCH, false);
    }

    public static boolean isFirstApplicationLaunch() {
        check();
        return settings.getBoolean(FLAG_IS_FIRST_APPLICATION_LAUNCH, true);
    }

    public static void viewInCards() {
        check();
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(VIEW_IN_CARDS, true);
        editor.putBoolean(VIEW_IN_LIST, false);
        editor.apply();
    }

    public static void viewInList() {
        check();
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(VIEW_IN_LIST, true);
        editor.putBoolean(VIEW_IN_CARDS, false);
        editor.apply();
    }

    public static boolean isViewInCards() {
        return settings.getBoolean(VIEW_IN_CARDS, true);
    }

    public static boolean isViewInList() {
        return settings.getBoolean(VIEW_IN_LIST, false);
    }
}
