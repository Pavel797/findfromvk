package ru.bigtail.model.answer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Павел on 16.07.2017.
 */

public class ModelAnswerUser{

    @SerializedName("result")
    @Expose
    private Integer result;

    @SerializedName("response")
    @Expose
    private ModelResponseUser response;

    @SerializedName("error_message")
    @Expose
    private String error_message;

    @SerializedName("error_code")
    @Expose
    private int error_code;

    public ModelAnswerUser() {
    }

    public ModelResponseUser getResponse() {
        return response;
    }

    public Integer getResult() {
        return result;
    }


    public String getErrorMessage() {
        return error_message;
    }

    public int getErrorCode() {
        return error_code;
    }
}
