package ru.bigtail.model.answer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ru.bigtail.model.items.ModelUser;

/**
 * Created by Павел on 16.07.2017.
 */

public class ModelResponseUser {

    @SerializedName("count")
    @Expose
    private long count;

    @SerializedName("items")
    @Expose
    private ArrayList<ModelUser> items;

    @SerializedName("history_id")
    @Expose
    private String history_id;

    @SerializedName("history_exist")
    @Expose
    private boolean history_exist;

    @SerializedName("offset")
    @Expose
    private Integer offset;

    public ModelResponseUser() {
    }

    public String getHistoryId() {
        return history_id;
    }

    public long getCount() {
        return count;
    }

    public ArrayList<ModelUser> getItems() {
        return items;
    }

    public String getHistory_id() {
        return history_id;
    }

    public Integer getOffset() {
        return offset;
    }

    public boolean isHistoryExist() {
        return history_exist;
    }
}
