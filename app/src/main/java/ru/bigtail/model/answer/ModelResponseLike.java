package ru.bigtail.model.answer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Павел on 04.08.2017.
 */

public class ModelResponseLike {

    @SerializedName("likes_count_total")
    @Expose
    private long likes_count_total;

    @SerializedName("is_liked")
    @Expose
    private Boolean is_liked;

    public ModelResponseLike() {
    }

    public Boolean getIsLiked() {
        return is_liked;
    }

    public long getLikesCountTotal() {
        return likes_count_total;
    }
}
