package ru.bigtail.model.answer;

/**
 * Created by Павел on 16.07.2017.
 */

public class ModelAnswerCheckLink {

    private Response response;

    public ModelAnswerCheckLink() {
    }

    public Response getResponse() {
        return response;
    }

    public class Response {
        private String type;
        private long object_id;

        public Response() {
        }

        public String getType() {
            return type;
        }

        public long getObject_id() {
            return object_id;
        }
    }
}
