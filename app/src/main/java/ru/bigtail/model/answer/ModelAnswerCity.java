package ru.bigtail.model.answer;

import ru.bigtail.model.forms.ModelCity;

/**
 * Created by Павел on 17.07.2017.
 */

public class ModelAnswerCity {

    Response response;

    public ModelAnswerCity() {
    }

    public Response getResponse() {
        return response;
    }

    public class Response {

        private int count;
        private ModelCity items[];

        public Response() {
        }

        public int getCount() {
            return count;
        }

        public ModelCity[] getItems() {
            return items;
        }
    }
}
