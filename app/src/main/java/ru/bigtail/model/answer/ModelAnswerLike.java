package ru.bigtail.model.answer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Павел on 04.08.2017.
 */

public class ModelAnswerLike {

    @SerializedName("result")
    @Expose
    private Integer result;

    @SerializedName("response")
    @Expose
    private ModelResponseLike response;

    @SerializedName("error_message")
    @Expose
    private String error_message;

    @SerializedName("error_code")
    @Expose
    private int error_code;

    public ModelAnswerLike() {
    }

    public Integer getResult() {
        return result;
    }

    public ModelResponseLike getResponse() {
        return response;
    }

    public String getErrorMessage() {
        return error_message;
    }

    public int getErrorCode() {
        return error_code;
    }
}
