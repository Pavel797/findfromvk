package ru.bigtail.model.answer;

import ru.bigtail.model.forms.ModelCountry;

/**
 * Created by Павел on 16.07.2017.
 */

public class ModelAnswerCountry {

    private Response response;

    public ModelAnswerCountry() {
    }

    public Response getResponse() {
        return response;
    }

    public class Response {

        private int count;
        private ModelCountry items[];

        public Response() {
        }

        public int getCount() {
            return count;
        }

        public ModelCountry[] getItems() {
            return items;
        }
    }

}
